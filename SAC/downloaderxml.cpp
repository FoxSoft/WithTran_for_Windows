#include "downloaderxml.h"
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QFile>
#include <QMessageBox>
#include <QtNetwork/QNetworkReply>
#include <QDomNode>
#include <QUrl>

QString DownloaderXml::LoadFilleWindow(QString patch)
{
    QString buf;
    QFile file(patch);
     if (!file.open(QIODevice::ReadOnly))
         return "";
     while(!file.atEnd())
     {
        buf += buf + file.readAll();
     }

     return buf;
}

QList<QStringList> DownloaderXml::parseTag(QDomNodeList &parent, QDomDocument &doc, QString tag)
{
    int i=0;
   QList<QStringList>  outList;
   if (tag == "search") i=1;
   parent = doc.elementsByTagName(tag);

       switch (i)
       {
       case 1:
           if (!parent.isEmpty())
           {
           QStringList Lfrom, Lto;
           QDomNodeList list = parent.at(0).childNodes();
               for (int i = 0; i < list.size(); ++i)
               {
                   QDomElement search = list.at(i).toElement();

                        if (search.tagName() == "from")
                        {
                            QDomNodeList fromsList = search.childNodes();
                            for (int j = 0; j < fromsList.size(); ++j)
                            {
                                  QDomElement from = fromsList.at(j).toElement();
                                  if (from.tagName() == "title")
                                  {

                                    NameStationFrom = from.text();
                                    Lfrom << NameStationFrom;
                                  }
                                  if (from.tagName() == "code")
                                  {
                                    codCity[NameStationFrom] = from.text();
                                    Lfrom << from.text();
                                  }
                            }
                        }


                        if (search.tagName() == "to")
                        {
                            QDomNodeList toList = search.childNodes();
                            for (int j = 0; j < toList.size(); ++j)
                            {
                                 QDomElement to = toList.at(j).toElement();
                                 if (to.tagName() == "title")
                                 {
                                    NameStationTo = to.text();
                                    Lto << NameStationTo;
                                 }
                                 if (to.tagName() == "code")
                                 {
                                    codCity[NameStationTo] = to.text();
                                    Lto << to.text();
                                 }

                            }
                        }
                    }
                return outList << Lfrom << Lto;
              qDebug() << outList.at(0).value(0);//обращение к Lfrom к коду города
                }
           break;
       default:
           if (!parent.isEmpty())
           {
               QStringList Lthreads, Lthread, Lfrom, Lto;
               //QString arrival("nill"), departure("nill"), dur;
               Lfrom << "" << ""; Lto << "" << ""; Lthread << "" << "";
               QDomNodeList list = parent.at(0).childNodes();
               for (int i = 0; i < list.size(); ++i)
               {
                   QDomElement elem = list.at(i).toElement();
                   if (elem.tagName() == "threads")
                   {
                               QDomNodeList threadsList = elem.childNodes();
                               for (int j = 0; j < threadsList.size(); ++j)
                               {
                                   QDomElement threads = threadsList.at(j).toElement();
                                  if (threads.tagName() == "arrival")
                                      Lthreads << threads.text();
                                  if (threads.tagName() == "duration")
                                      Lthreads << threads.text();
                                  if (threads.tagName() == "departure")
                                      Lthreads << threads.text();


                                        if (threads.tagName() == "thread")
                                        {
                                            QDomNodeList threadList = threads.childNodes();
                                            for (int j = 0; j < threadList.size(); ++j)
                                            {
                                                QDomElement thread = threadList.at(j).toElement();
                                              if (thread.tagName() == "title")
                                                 Lthread.replace(0, thread.text());
                                              if (thread.tagName() == "number")
                                                 Lthread.replace(1, thread.text());
                                            }
                                         }

                                        if (threads.tagName() == "from")
                                        {
                                            QDomNodeList fromList = threads.childNodes();
                                            for (int j = 0; j < fromList.size(); ++j)
                                            {
                                             QDomElement from = fromList.at(j).toElement();
                                              if (from.tagName() == "title")
                                                Lfrom.replace(0, from.text());
                                             if (from.tagName() == "code")
                                                Lfrom.replace(1, from.text());
                                            }
                                         }

                                        if (threads.tagName() == "to")
                                        {
                                            QDomNodeList toList = threads.childNodes();
                                            for (int j = 0; j < toList.size(); ++j)
                                            {
                                             QDomElement to = toList.at(j).toElement();
                                              if (to.tagName() == "title")
                                                Lto.replace(0, to.text());
                                             if (to.tagName() == "code")
                                                Lto.replace(1, to.text());
                                            }
                                        }
                               }
                   }
               }
               outList << Lthreads <<Lthread << Lfrom << Lto;
           }

           break;
       }

       int item=0;
       while (item < outList.at(0).count())
       {
           ListTime[outList.at(0).at(item)] = outList.at(0).at(item+2);
           //время в пути
           duration << (QString)outList.at(0).at(item+1);
           item+=3;
       }

        NameStationFrom = outList.at(2).at(0);
       //имя ст. прибытия
        NameStationTo = outList.at(3).at(0);

        //название рейса
        NameReys = outList.at(1).at(0);
       //номер рейса
        number = outList.at(1).at(1);
       //код города по ключу названия города
        codCity[NameStationFrom] = outList.at(2).at(1); ;
        codCity[NameStationTo] = outList.at(3).at(1);

       return outList;

}

QString DownloaderXml::getAllCodeStation()
{
    QString("https://api.rasp.yandex.net/v3.0/stations_list/"
                       "?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&lang=RU");
}

QString DownloaderXml::setCodeCity(QString From, QString To)
{
    return QString("https://api.rasp.yandex.net/v1.0/search/"
                   "?apikey=86973449-b547-417e-95da-e70f87ef8748&format="
                      "xml&from="+From+"&to="+To+"&lang=ru&page=1");
}

void DownloaderXml::parseXmlRasp()
{
    QFile output ("rasp.xml");
    QDomElement eElem;
    QDomDocument doc;
    if(output.open(QIODevice::ReadOnly))
    {
        if(doc.setContent(&output))
        {
            QDomElement domElement= doc.documentElement();
        }
        output.close();
    }
       QString SelectedAttr = "do";
       QDomNodeList ParentTag = doc.elementsByTagName("threads");
       QDomNodeList titleFrom = doc.elementsByTagName("from");

       for (int i = 0; i < ParentTag.length(); i ++)
       {
          QDomNode node = ParentTag.at(i);
          QDomNode node2 = titleFrom.at(i);


           if(!node.isNull())
           {
                eElem=node.firstChildElement("thread");
              // QDomElement elemToFrom=node2.firstChildElement("from");

               //ui->textEdit_2->setText(ui->textEdit_2->toPlainText()+"Откуда: "+ getNode2("title", node2.toElement()) +"\n");

              // ui->textEdit_2->setText(ui->textEdit_2->toPlainText()+ "Куда: "+ getNode("title", node.toElement()) +"\n");

             //  ui->textEdit_2->setText(ui->textEdit_2->toPlainText()+"Отправка:\n"+ getNode("departure", node.toElement()) +"\n");

              // ui->textEdit_2->setText(ui->textEdit_2->toPlainText()+ "Прибытие:\n"+ getNode("arrival", node.toElement()) +"\n\n");

               ListTime[getNode("arrival", node.toElement())]=getNode("departure", node.toElement());

               NameReys=getNode2("title", eElem);
               NameStationFrom=getNode2("title", node2.toElement());
               NameStationTo=getNode("title", node.toElement());
           }

            //экспорт в файл, с готовым форматированием
//           QFile file ("rasp_remake.xml");
//           //QString newxml = ui->textEdit_2->toPlainText();
//           if (file.open(QIODevice::WriteOnly))
//           {
//               QTextStream stream(&file);
//               stream.setCodec( "UTF-8 ");
//               stream << newxml;
//           }
//           file.close();
       }



       qDebug() << NameReys << NameStationFrom << NameStationTo;
}

//конструктор
DownloaderXml::DownloaderXml(QObject *parent) : QObject(parent)
{
    // Инициализируем менеджер ...
        manager = new QNetworkAccessManager();
        // ... и подключаем сигнал о завершении получения данных к обработчику полученного ответа
        connect(manager, &QNetworkAccessManager::finished, this, &DownloaderXml::onResult);
}

QString DownloaderXml::getNode2(QString sTag, QDomElement element)
{
    QDomNodeList nodeList=element.elementsByTagName(sTag).item(0).childNodes();  //.getChildNodes();
    QDomNode nValue = nodeList.item(0);
    return nValue.nodeValue();  //NodeValue();
}

QString DownloaderXml::getNode(QString sTag, QDomElement element)
{
    QDomNodeList nodeList=element.elementsByTagName(sTag).item(0).childNodes();
            QDomNode nValue = nodeList.item(0);
            return nValue.nodeValue();
}

//создает файл xml
void DownloaderXml::getData()
{
    //c10728
    //c10734
    // берем адрес введенный в текстовое поле
    QUrl url(setCodeCity("c10728","c213"));

    QNetworkRequest request(url);

    // Выполняем запрос, получаем указатель на объект
    // ответственный за ответ
    QNetworkReply* reply=  manager->get(request);
}

void DownloaderXml::onResult(QNetworkReply *reply)
{

    // Если в процесе получения данных произошла ошибка
    if(reply->error()== QNetworkReply::NoError)
    {

        if(reply->hasRawHeader("Location"))
        {
            // Запрос по перенаправлению
            QString locationUrl= reply->header(QNetworkRequest::LocationHeader).toString();
            QNetworkRequest request(locationUrl);
            QNetworkReply* reply=  manager->get(request);
            connect(manager, &QNetworkAccessManager::finished, this, &DownloaderXml::onResult);
        }
        else
        {
        // В противном случае создаём объект для работы с файлом
        QFile file("rasp.xml");
        // Создаём файл или открываем его на перезапись ...
        if(file.open(QIODevice::WriteOnly))
        {
           // file.write(reply->readAll());  // ... и записываем всю информацию со страницы в файл
            QByteArray content= reply->readAll();
                    file.write(content); // пишем в файл

            file.close();
        emit onReady(); // Посылаем сигнал о завершении получения файла
        }
    }
}
    else
    reply->deleteLater();
}

//подкючение по url
void DownloaderXml::readFile()
{
    // берем адрес введенный в текстовое поле
    QUrl url(setCodeCity("c10728", "c213"));

    QNetworkRequest request(url);

    // Выполняем запрос, получаем указатель на объект
    // ответственный за ответ
    QNetworkReply* reply=  manager->get(request);

//    // Подписываемся на сигнал о готовности загрузки
//    connect( reply, SIGNAL(finished()),
//             this, SLOT(replyFinished()) );

}

//слот
void DownloaderXml::readFileXml()
{
//    qDebug() << XmlData;
//    QFile file("rasp.xml");
//    if (!file.open(QIODevice::ReadOnly | QIODevice::WriteOnly)) // Открваем файл, если это возможно
//            return; // если открытие файла невозможно, выходим из слота
//    // в противном случае считываем данные и устанавилваем их в textEdit
//    XmlData=file.readAll();
//    file.close();


    QFile in( "rasp.xml" );
        if( in.open( QIODevice::ReadOnly ) ) {
            QTextStream stream( &in );
            stream.setCodec( "UTF-8 ");
            XmlData=in.readAll();
            in.close();
        }

}
