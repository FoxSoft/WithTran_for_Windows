#ifndef ALLCODESTATION_H
#define ALLCODESTATION_H

#include <QObject>

class allCodeStation
{
public:
    QString nameStation, codeStation, nameRegion, codeRegion, typeTransport, typeStation, nameCity, codeCity;
    allCodeStation();
    allCodeStation(QString nameRegion, QString codeRegion, QString nameCity, QString codeCity,
                   QString nameStation, QString codeStation, QString typeTransport, QString typeStation);

    QString getNameStation() const;
    void setNameStation(const QString &value);
    QString getCodeStation() const;
    void setCodeStation(const QString &value);
    QString getNameRegion() const;
    void setNameRegion(const QString &value);
    QString getCodeRegion() const;
    void setCodeRegion(const QString &value);
    QString getTypeTransport() const;
    void setTypeTransport(const QString &value);
    QString getTypeStation() const;
    void setTypeStation(const QString &value);
    QString getNameCity() const;
    void setNameCity(const QString &value);
    QString getCodeCity() const;
    void setCodeCity(const QString &value);
    void clear();
};

#endif // ALLCODESTATION_H
