#ifndef PARSEXML_H
#define PARSEXML_H
#include "datebase.h"

#include <QObject>
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QUrl>
#include <QDebug>
#include <QXmlStreamReader>
#include <QDomNode>

class ParseXml : public QObject
{
    Q_OBJECT
public:
    explicit ParseXml(QObject *parent = 0);

    void parseXmlRasp();
    //расписание от А до В
    QList<QStringList> parseTag();
    //все остановки по маршруту
    QList<QStringList> parseListThread(QString tag);
    //рейса проходящие через эту остановку
    QList<QStringList> parseSchedule(QString tag);

    void ParseAllCodeStation();


signals:
    MoveCOdeCityComplete();
    recordToDBComplete();

public slots:
    //коды всех городов
    parseCode();
    recordToDB();

public:
    //данные xml
    QString XmlData;
    //время отправления
    QList<QStringList> ListTime;
    //имя ст. отправки
    QList<QString> NameStationFrom;
    //имя ст. прибытия
    QList<QString> NameStationTo;
    //название рейса
    QList<QString> NameReys;
    //время в пути
    QList<QString> duration;
    //номер рейса
    QList<QString> number;
    //код города по ключу названия города
    QList<QStringList> codCity;
    //uid - номер нитки маршрута
    QList<QString> UID;
    //тип транспорта
    QList<QString> typeTransport;
    //коды всех городов рф
    QMultiMap<QString, QString> AllCodeCity;
    //дни когда ходят автобусы
    QList<QString> Day;



    Datebase *db;

};

#endif // PARSEXML_H
