#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "schedulesubjects.h"
#include "obj.h"
#include "downloaderxml.h"
#include "message.h"
#include "allcodestation.h"

#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>
#include <QtGui>
#include <QPushButton>
#include <QProcess>
#include <QtQuickWidgets/QQuickWidget>
#include <QGuiApplication>
#include <QtConcurrent/QtConcurrent>



MainWindow::MainWindow(QWidget *parent):QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint);
  this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAttribute(Qt::WA_NativeWindow);

    Tray = new SystemTray(this);

    //ScheduleSubjects *wgt = new ScheduleSubjects("/schedule.xls", 1, 8, 4, 37);
   // wgt->setTimeFirstSubject("пятница.10.12");
  //  ui->horizontalLayout->addWidget(wgt);


    db = new Datebase(this);
    if (!db->createConnection("list_station.db"))
    {
       QMessageBox::critical(NULL,QObject::tr("Ошибка"),"Нет подключения к БД 8(");
    }

    //слот загружает в комбобоксы варинты названий городов
    loadVarintSearch();

    downloader = new Downloader(this); // Инициализируем Downloader

    parsexml = new ParseXml(this);

    message = new Message(this);

    alarmclock = new AlarmClock(this);

    //расписание успешно загрузилось, тогда заносим в файлы названия городов
    connect(downloader, &Downloader::DownloadComplete, this, &MainWindow::writeVarintSearch);

    //коннекты кнопок, которые наверху
    connect(ui->pushButton_3,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->pushButton_2,&QPushButton::clicked,this,QWidget::showMinimized);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mouseMoveEvent(QMouseEvent *e)
{
    if(myPos.x()>=0 && e->buttons() && Qt::LeftButton)
    {
        QPoint diff=e->pos()-myPos;
        QPoint newpos=this->pos()+diff;
        this->move(newpos);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *e)
{
    myPos=e->pos();
}

void MainWindow::mouseReleaseEvent(QMouseEvent *e)
{
    myPos=QPoint(-1,1);
}

void MainWindow::loadVarintSearch()
{
    QStringList dat;
    //from, to
    QFile file("historySearchCity.sac");
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        while(!in.atEnd())
        {
            dat.append(in.readLine());
        }
        file.close();
    } else qDebug() << "not file";


//    QMap<QString, QString>::Iterator in = map.begin();
//    for(; in != map.end(); in++)
//    {
//        ui->comboBoxFrom->setCurrentText(in.key());
//        ui->comboBoxFrom->setCurrentText(in.value());
    //}

    QList<QStringList> list = db->recordsTable(dat.at(dat.count()-2)+"_"+dat.at(dat.count()-1));
    o = new obj(this);
    o->createDataList(list);

    QQmlContext *ctxt = ui->quickWidget->rootContext();
    ui->quickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    ctxt->setContextProperty("myModel", QVariant::fromValue(o->dataList));
    ui->quickWidget->setSource(QUrl("qrc:view.qml"));
}

void MainWindow::writeVarintSearch()
{
        QFile fileTo("historySearchCity.sac");
        if(fileTo.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
        {
            QTextStream(&fileTo) << searchFrom << searchTo;
            fileTo.close();
        } else qDebug() << "error file";
}
//слот
void MainWindow::blockButton()
{
    ui->pushButton->setEnabled(true);
}

void MainWindow::on_pushButton_clicked()
{
   //  QtConcurrent::run(parsexml, ParseXml::parseCode);
  //    QMap<QString, QString>::iterator it = parsexml->AllCodeCity.begin();
  //    for (;it != parsexml->AllCodeCity.end(); ++it)
  //    {
  //        //qDebug() << "Name:" << it.key() << " Phone:" << it.valued;

  //        QtConcurrent::run(db,Datebase::run, db->insert(it.key(), it.value()));
  //    }


      ui->pushButton->setEnabled(false);
      QtConcurrent::run(parsexml, ParseXml::recordToDB);
      connect(parsexml, &ParseXml::recordToDBComplete, this, &MainWindow::blockButton);

     // qDebug() << "\n\n" << db->findCode("Луховицы");

}

void MainWindow::on_pushButton_4_clicked()
{
//    qDebug() << "\nNameFrom " << parsexml->NameStationFrom.count();
//    qDebug() << "\nNameTo " << parsexml->NameStationTo.count();
//    qDebug() << "\nNameReys" << parsexml->NameReys.count();
//    qDebug() << "\nUID" << parsexml->UID.count();
//    qDebug() << "\nListTime " << parsexml->ListTime.count();
//    qDebug() << "\nduron " << parsexml->duration.count();
//    qDebug() << "\nnumber " << parsexml->number.count();
//    qDebug() << "\ncodCity " << parsexml->codCity.count();
    //qDebug() << db->hasTable("Зарайск_Коломна");
    downloader->getAllCodeStation();
}

void MainWindow::on_btn_clicked()
{
    if((ui->comboBoxFrom->currentText().isEmpty() | ui->comboBoxTo->currentText().isEmpty())|
            (ui->comboBoxFrom->currentText() == ui->comboBoxTo->currentText()))
    {
        message->setPopupText("<h2><font color='red'; >Внимание!</font></p></h2>"
                              "<p>Заполните все поля правильно ...");
        message->show();
    }
    else
    {
//        if(!db->hasTable(ui->comboBoxFrom->currentText()+"_"+ui->comboBoxTo->currentText()))
//        {
            searchFrom = ui->comboBoxFrom->currentText() + "\n";
            searchTo = ui->comboBoxTo->currentText() + "\n";
            downloader->getXMLRaspFromTo(db->findCode(ui->comboBoxFrom->currentText()), db->findCode(ui->comboBoxTo->currentText()));

//        }
//        else
//        {
//            message->setPopupText("<h2><font color='red'; >Внимание!</font></p></h2>"
//                                     "<p>Заполните все поля ...");
//            message->show();
//            QMessageBox::information(0, "Внимание!", "У вас уже есть рассписание этого маршрута. "
//                                                     "Вы уверены, что хотите его загрузить заново?",
//                                     QMessageBox::Yes|QMessageBox::No);
//        }
      //  qDebug() << db->hasTable("Зарайск_Коломна"); //db->hasTable(ui->comboBoxFrom->currentText()+"_"+ui->comboBoxTo->currentText());
    }
}

void MainWindow::on_pushButton_5_clicked()
{

//    if(ui->listWidget->count()>0)
//    {
//        for(int i=0; i<ui->listWidget->count(); i++)
//        {
//            ui->listWidget->item(i)->setIcon(QPixmap(":/AlarmlClock/poweroff.png"));
//            ui->listWidget->item(i)->setStatusTip("выключен");
//        }
//    }

    if(ui->listWidget->count()>0)
    {

        ui->listWidget->item(ui->listWidget->count()-1)->setIcon(QPixmap(":/AlarmlClock/poweroff.png"));
        ui->listWidget->item(ui->listWidget->count()-1)->setToolTip("выключен");
    }

    QListWidgetItem* item = new QListWidgetItem();
    item->setIcon(QPixmap(":/AlarmlClock/poweron.png"));
    item->setText(ui->timeEdit_2->text());
    item->setToolTip("включен");
    ui->listWidget->addItem(item);
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
   // ui->timeEdit_2->setTime(&item->text());

    for(int i=0; i<ui->listWidget->count(); i++)
    {
        ui->listWidget->item(i)->setIcon(QPixmap(":/AlarmlClock/poweroff.png"));
        ui->listWidget->item(i)->setToolTip("выключен");
    }

            item->setIcon(QPixmap(":/AlarmlClock/poweron.png"));
            item->setToolTip("включен");
            item->toolTip()="активирован";

    qDebug() << item->text();
}

void MainWindow::on_radioButton_clicked()
{
    ui->radioButton->setIcon(QPixmap(":/AlarmlClock/checkon.png"));
    ui->radioButton_2->setChecked(false);
}

void MainWindow::on_radioButton_2_clicked()
{
     ui->radioButton->setChecked(false);
}

void MainWindow::on_checkBox_clicked()
{
    ui->statusAlarmClock->checkState() ? Qt::Checked : Qt::Unchecked;
}

void MainWindow::on_statusAlarmClock_clicked()
{
    if(ui->statusAlarmClock->checkState()==Qt::Checked)
    {
        ui->radioButton->setCheckable(false);
        ui->radioButton_2->setCheckable(false);
    }
    else
    {
        ui->radioButton->setCheckable(true);
        ui->radioButton_2->setCheckable(true);
    }
}

void MainWindow::on_pushButtonParsingCode_clicked()
{
    parsexml->ParseAllCodeStation();
//    allCodeStation allCode;
//    allCode.setCodeCity("c4564");
//    allCode.setCodeRegion("r554564");
//    allCode.setCodeStation("s766264");
//    allCode.setNameCity("Quer");
//    allCode.setNameRegion("Pooi");
//    allCode.setNameStation("a1");
//    allCode.setTypeStation("bus");
//    allCode.setTypeTransport("bus");
//    QList<allCodeStation> listCodeStation;
//    listCodeStation.append(allCode);
//    //listCodeStation.append(new allCodeStation("d","d","d","d","d","d","d","d"));
//    qDebug() << listCodeStation.at(0).getCodeCity();
    //db->setCodeAllStation("d","d","d","d","d","d","d","d");
}

void MainWindow::on_pushButton_6_clicked()
{
//    QMap<QString, QString> map;
//    QStringListModel model;
//    QList<QString> list;
//    QItemSelectionModel selection(&model);
//    map = db->getListObl();
//    QMap<QString, QString>::iterator iterator = map.begin();
//    for(; iterator != map.end(); iterator++)
//    {
//        qDebug() << iterator.key();
//    }
//   qDebug() << db->getNameRegion("r1");
//     qDebug() << "--------------";
    if (!db->createConnection("list_station.db"))
    {
       QMessageBox::critical(NULL,QObject::tr("Ошибка"),"Нет подключения к БД 8(");
    }

      qDebug() << db->getObl("М");



      db->searchStation("пер");
}
