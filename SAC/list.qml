import QtQuick 2.0

ListModel
{
    ListElement
    {
        artist: "Amaranthe"
        album: "Amaranthe"
        year: 2011
        cover: "qrc:///covers/Amaranthe.png"
    }
    ListElement
    {
        artist: "Dark Princess"
        album: "Without You"
        year: 2005
        cover: "qrc:///covers/WithoutYou.png"
    }
    ListElement
    {
        artist: "Within Temptation"
        album: "The Unforgiving"
        year: 2011
        cover: "qrc:///covers/TheUnforgiving.png"
    }
}
