#ifndef OBJ_H
#define OBJ_H

#include <QObject>

#include <QtWidgets/QLabel>

class obj : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString NameStationFrom READ NameStationFrom WRITE setNameStationFrom NOTIFY NameStationFromChanged)
    Q_PROPERTY(QString NameStationTo READ NameStationTo WRITE setNameStationTo NOTIFY NameStationToChanged)
    Q_PROPERTY(QString TimeFrom READ TimeFrom WRITE setTimeFrom NOTIFY TimeFromChanged)
    Q_PROPERTY(QString TimeTo READ TimeTo WRITE setTimeTo NOTIFY TimeToChanged)
    Q_PROPERTY(QString Status READ Status WRITE setStatus NOTIFY StatusChanged) 
    Q_PROPERTY(QString NameReys READ NameReys WRITE setNameReys NOTIFY NameReysChanged)
    Q_PROPERTY(QString Duration READ Duration WRITE setDuration NOTIFY DurationChanged)
    Q_PROPERTY(QString Numbers READ Numbers WRITE setNumbers NOTIFY NumbersChanged)
    Q_PROPERTY(QString Days READ Days WRITE setDays NOTIFY DaysChanged)
    Q_PROPERTY(bool statusBusBool READ statusBusBool WRITE setstatusBusBool NOTIFY statusBusBoolChanged)



public:
    obj(QObject *parent=0);

    obj(const QString &NameStationFrom, const QString &NameStationTo, const QString &TimeFrom, const QString &TimeTo, QString &Status,
        const QString &NameReys,const QString &Duration, const QString &Numbers, const QString &Days, QObject *parent=0);

    obj(const QList<QStringList>& list, QObject *parent);

    void createDataList(QList<QStringList> list);
    QString FormatDurationTotime(QString Duration);
    QString positionTextReys(QString reys);

    QString NameStationFrom() const;
    QString NameStationTo() const;
    QString TimeFrom() const;
    QString TimeTo() const;
    QString Status() const;
    QString NameReys() const;
    QString Duration() const;
    QString Numbers() const;
    QString Days() const;
    bool statusBusBool() const;

    void setNameStationFrom(const QString &NameStationFrom);
    void setNameStationTo(const QString &NameStationTo);
    void setTimeFrom(const QString &TimeFrom);
    void setTimeTo(const QString &TimeTom);
    void setStatus(const QString &Status);
    void setNameReys(const QString &NameReys);
    void setDuration(const QString &Duration);
    void setNumbers(const QString &Numbers);
    void setDays(const QString &Days);
    void setstatusBusBool(const bool &statusBusBool);

signals:
    void NameStationFromChanged();
    void NameStationToChanged();
    void TimeFromChanged();
    void TimeToChanged();
    void StatusChanged();
    void NameReysChanged();
    void DurationChanged();
    void NumbersChanged();
    void DaysChanged();
    void statusBusBoolChanged();


public:
    // разделитель : меняем на .
    QString FormatTime(QString time);
    // например 3:00 переводим в сек
    int FormatTimeToSec(QString time);
    //отнимает сек от реального времени
    QString minusTimeState(QString time);
    //ф-ия для поля статус автобуса
    QString statusBus(QString time);

public:
    QList<QObject*> dataList;
    QTimer* timer;
    //ушел автобус или нет
    bool m_statusBusBool = false;

    QString m_NameStationFrom;
    QString m_NameStationTo;
    QString m_TimeFrom;
    QString m_TimeTo;
    QString m_Status;
    QString m_NameReys;
    QString m_Duration;
    QString m_Numbers;
    QString m_Days;





public slots:
    void timer_overflow();

};

#endif // OBJ_H
