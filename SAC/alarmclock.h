#ifndef ALARMCLOCK_H
#define ALARMCLOCK_H

#include <QMediaPlayer>
#include <QObject>
#include <qtimer.h>
#include <QMediaPlaylist>

class AlarmClock : public QObject
{
    Q_OBJECT
public:
    explicit AlarmClock(QObject *parent = 0);
    void setVolume(int value);
    void playListPlay();
    void addToPlayList(QList<QString> file);

signals:

private:
    QTimer *timer;
    QMediaPlayer *player;
    QMediaPlaylist* playlist;//в будующем

public slots:

    void timer_overflow();
    void play();
    void stop();

};

#endif // ALARMCLOCK_H
