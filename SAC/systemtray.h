#pragma once

#include <QLabel>
#include <QMainWindow>

class QSystemTrayIcon;
class QMenu;

// ======================================================================
class SystemTray : public QMainWindow {
Q_OBJECT
private:
    QSystemTrayIcon* m_ptrayIcon;
    QMenu*           m_ptrayIconMenu;
    bool             m_bIconSwitcher;


protected:
    virtual void closeEvent(QCloseEvent*);

public:
    SystemTray(QMainWindow* pwgt = nullptr);

public slots:
    void slotShowHide();
    void slotShowMessage();
    void slotChangeIcon();
    void slotSleepTime();
};

