import QtQuick 2.0

ListView
{

    id:idView;
    width: 100
    height: 200

    Text
    {
        id: idTitle
        x: 8
        y: 8
        text: qsTr("title")
    }

    Item
    {
        id: idItem
        x: 4
        y: 27
        width: 92
        height: 103
        Text
        {
            id: idText
            x: 8
            y: 43
            text: qsTr("text")
        }
        Text
        {
            id: idText2
            x: 8
            y: 81
            text: qsTr("text")
        }
        Text
        {
            id: idText3
            x: 8
            y: 62
            text: qsTr("text")
        }
    }

}
