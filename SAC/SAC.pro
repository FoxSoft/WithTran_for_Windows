#-------------------------------------------------
#
# Project created by QtCreator 2017-07-28T11:42:13
#
#-------------------------------------------------

QT       += core gui sql qml quick network xml xmlpatterns multimedia concurrent multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += quickwidgets

TARGET = SAC
TEMPLATE = app

CONFIG  += qaxcontainer axcontainer
CONFIG  += c++11



SOURCES  += main.cpp\
            mainwindow.cpp \
            schedulesubjects.cpp \
            obj.cpp \
            downloaderxml.cpp \
            downloader.cpp \
            systemtray.cpp \
            message.cpp \
            datebase.cpp \
            parsexml.cpp \
            alarmclock.cpp \
    allcodestation.cpp \
    settingrasp.cpp \
    settingwindow.cpp



HEADERS  += mainwindow.h \
            schedulesubjects.h \
            obj.h \
            downloaderxml.h \
            downloader.h \
            systemtray.h \
            message.h \
            datebase.h \
            parsexml.h \
            alarmclock.h \
    allcodestation.h \
    settingrasp.h \
    settingwindow.h

FORMS    += mainwindow.ui \
    form.ui \
    settingwindow.ui

DISTFILES += \
        list.qml \
    test.qml
DISTFILES += \
        view.qml

RESOURCES += \
    rsc.qrc

