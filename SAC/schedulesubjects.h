#ifndef SCHEDULESUBJECTS_H
#define SCHEDULESUBJECTS_H

#include <QTableWidget>
#include <QTextEdit>
#include <QString>
#include <QHBoxLayout>
#include <QStringList>
#include <QHeaderView>
#include <QAxObject>
#include <QtGui>

class ScheduleSubjects : public QWidget
{
    Q_OBJECT
    QTableWidget* tableWgt;
    QTextEdit* textEdit;
    void deleteEmptyCell();
    void autoSize();
    QString timeFirstSubject(const QString&);
public:
    explicit ScheduleSubjects(const QString&, int x0, int y0, int x, int y);
    void setTimeFirstSubject(const QString&);
    void setTimeFirstSubject(const QDate&);
   // void checkUpdate();
    QString timeSubject;
signals:

public slots:
    //readyUpdate();
};

#endif // SCHEDULESUBJECTS_H
