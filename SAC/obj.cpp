#include "obj.h"
#include <QDebug>
#include <QtGuiDepends>
#include <QString>

obj::obj(QObject *parent) : QObject(parent)
{

}

obj::obj(const QString &NameStationFrom, const QString &NameStationTo, const QString &TimeFrom,
         const QString &TimeTo, QString &Status, const QString &NameReys,
         const QString &Duration, const QString &Numbers, const QString &Days, QObject *parent)
    : QObject(parent), m_NameStationFrom(NameStationFrom), m_NameStationTo(NameStationTo), m_TimeFrom(TimeFrom), m_TimeTo(TimeTo),
      m_Status(Status), m_NameReys(NameReys), m_Duration(Duration), m_Numbers(Numbers), m_Days(Days)
{
    timer = new QTimer;
    QObject::connect(timer,SIGNAL(timeout()), this, SLOT(timer_overflow()));
    timer->start(1000);
}


void obj::createDataList(QList<QStringList> list)
{


    for(int i = 0; i < list.at(0).count(); i++)
    {
        m_NameStationFrom = list.at(1).at(i);
        m_NameStationTo = list.at(2).at(i);
        m_TimeFrom = list.at(3).at(i);
        m_TimeTo = list.at(4).at(i);
        m_NameReys = list.at(8).at(i);
        m_Duration = FormatDurationTotime(list.at(5).at(i));
        m_Numbers = list.at(0).at(i);
        m_Days = positionTextReys(list.at(6).at(i));

        dataList.append(new obj(m_NameStationFrom, m_NameStationTo,
                                m_TimeFrom, m_TimeTo,
                                m_Status, m_NameReys, m_Duration,m_Numbers, m_Days));

    }
//   for(int i = 0; i < list.at(0).count(); i++)
//   {
//       m_NameStationFrom = list.at(1).at(i);
//       m_NameStationTo = list.at(2).at(i);
//       m_TimeFrom = list.at(3).at(i);
//       m_TimeTo = list.at(4).at(i);
//       m_Numbers = list.at(0).at(i);
//       m_NameReys = list.at(9).at(i);
//       m_Duration = list.at(5).at(i);
//       m_Days = list.at(6).at(i);

//    int i=0;
//    while (i < list.at(0).count())
//    {
//        dataList.append(new obj(list.at(1).at(i), list.at(2).at(i),
//                                    list.at(3).at(i), list.at(4).at(i),
//                                    list.at(1).at(i), list.at(9).at(i), list.at(5).at(i), list.at(0).at(i), list.at(6).at(i)));
//        i++;
//    }



//    dataList.append(new obj(m_NameStationFrom, m_NameStationTo,
//                            m_TimeFrom, m_TimeTo,
//                            m_Status, m_NameReys, m_Duration, m_Numbers, m_Days));
//   }

//    dataList.append(new obj(m_NameStationFrom, m_NameStationTo,
//                            "22:00", m_TimeTo,
//                            m_Status, m_NameReys, m_Duration, m_Numbers, m_Days));

}

QString obj::NameStationFrom() const
{
    return m_NameStationFrom ;
}

QString obj::NameStationTo() const
{
    return m_NameStationTo;
}

QString obj::TimeFrom() const
{
    return m_TimeFrom;
}

QString obj::TimeTo() const
{
    return m_TimeTo;
}

QString obj::Status() const
{
    return m_Status;
}

QString obj::NameReys() const
{
    return m_NameReys;
}

QString obj::Duration() const
{
    return m_Duration;
}

QString obj::Numbers() const
{
    return m_Numbers;
}

QString obj::Days() const
{
    return m_Days;
}

QString obj::positionTextReys(QString reys)
{
    QStringList list = reys.split(",");
    QString result;
    QString s = "<font color='blue'>Рейсы:  </font>";


    for(int i = 0; i < list.count(); i++)
    {
        s += list.at(i)+",";

        if(s.count() > 85 && reys.count() > 85)
        {
            s.chop(1);
            result += s + "<br/>" /*"\n"*/;
            s = "<font color='blue'>Рейсы:  </font>";
        }
    }

    s.chop(1);

    if(!result.isEmpty())
        result += s;
    else
    result += s;
  return result;
}


QString obj::FormatDurationTotime(QString Duration)
{
    int sec = Duration.toFloat();
    QList<int> ListTime;
    QString result;
    int hour = sec/3600;
    int minute = (sec - hour*3600)*0.016666666667;
    if(hour == 0)
    {
        result = QString::number(minute) + " мин.";
        return result;
    }
    else
    result = QString::number(hour) + " ч. "+QString::number(minute) + " мин.";
    return result;
}

bool obj::statusBusBool() const
{
    return m_statusBusBool;
}

void obj::setNameStationFrom(const QString &NameStationFrom)
{
    if (NameStationFrom != m_NameStationFrom ) {
        m_NameStationFrom = NameStationFrom;
        emit NameStationFromChanged();
    }
}

void obj::setNameStationTo(const QString &NameStationTo)
{
    if (NameStationTo!= m_NameStationTo) {
        m_NameStationTo = NameStationTo;
        emit NameStationToChanged();
    }
}

void obj::setTimeFrom(const QString &TimeFrom)
{
    if (TimeFrom != m_TimeFrom) {
        m_TimeFrom = TimeFrom;
        emit TimeFromChanged();
    }
}

void obj::setTimeTo(const QString &TimeTo)
{
    if (TimeTo != m_TimeTo) {
        m_TimeTo = TimeTo;
        emit TimeToChanged();
    }
}

void obj::setStatus(const QString &Status)
{
    if (Status != m_Status) {
        m_Status = Status;
        emit StatusChanged();
    }
}

void obj::setNameReys(const QString &NameReys)
{
    if (NameReys != m_NameReys) {
        m_NameReys = NameReys;
        emit NameReysChanged();
    }
}

void obj::setDuration(const QString &Duration)
{
    if (Duration != m_Duration) {
        m_Duration = Duration;
        emit DurationChanged();
    }
}

void obj::setNumbers(const QString &Numbers)
{
    if (Numbers != m_Numbers) {
        m_Numbers = Numbers;
        emit NumbersChanged();
    }
}

void obj::setDays(const QString &Days)
{
    if (Days != m_Days) {
        m_Days = Days;
        emit DaysChanged();
    }
}

void obj::setstatusBusBool(const bool &statusBusBool)
{
    if (statusBusBool != m_statusBusBool) {
        m_statusBusBool= statusBusBool;
        emit statusBusBoolChanged();
    }
}

QString obj::FormatTime(QString time)
{
        time.remove(4,3);
        time.insert(2,".");
        time.remove(3,1);
        return time;
}

int obj::FormatTimeToSec(QString time)
{
        int result=0;
        int hour = time.left(2).toInt()*3600;
        int minute = time.right(2).toInt()*60;
        result = hour+minute;
        return result;
}

QString obj::minusTimeState(QString time)
{
        int timeSec = FormatTimeToSec(time);
        return QTime::currentTime().addSecs(-timeSec).toString("hh:mm") ;//t.currentTime().addSecs(-timeSec).toString("hh:mm");
}

QString obj::statusBus(QString time)
{
        QTime t = QTime::fromString(FormatTime(time), "h.m");

        int hour = t.hour() - QTime::currentTime().hour();
        int minute = t.minute() - QTime::currentTime().minute();
        t.setHMS(hour, minute, 0);

        if ((hour >= 0 && minute >= 0)||(hour > 0 && minute <= 0))
        {
            setstatusBusBool(true);
            return "отправка через " + (QString::number(hour) == "0" ? "" : QString::number(hour)+" ч. ")
                    + QString::number(minute < 0 ? -minute : minute) + " мин.";
        }
        else
        {
            if(hour == 0)
            {
                setstatusBusBool(false);
                return "ушел " + QString::number((minute)<0 ? -minute:minute) + " мин. назад";
            }
            else return "ушел ";
        }
}

void obj::timer_overflow()
{
    setStatus(statusBus(m_TimeFrom));
}


