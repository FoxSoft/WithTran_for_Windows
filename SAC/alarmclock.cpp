#include "alarmclock.h"

AlarmClock::AlarmClock(QObject *parent) : QObject(parent)
{
    player = new QMediaPlayer;
    playlist = new QMediaPlaylist();
    player->setVolume(100);
}

void AlarmClock::playListPlay()
{
    player = new QMediaPlayer;
    player->setPlaylist(playlist);
    player->play();
}

void AlarmClock::addToPlayList(QList<QString> fileList)
{
    for(int i=0; i<fileList.count(); i++)
    {
        playlist->addMedia(QUrl(fileList.at(i)));
    }

    playlist->setCurrentIndex(1);
}

void AlarmClock::timer_overflow()
{

}

void AlarmClock::play()
{

}

void AlarmClock::stop()
{

}
