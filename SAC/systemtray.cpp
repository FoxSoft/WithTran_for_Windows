
#include <QtWidgets>
#include "SystemTray.h"
#include "mainwindow.h"

// ----------------------------------------------------------------------
SystemTray::SystemTray(QMainWindow *pwgt): QMainWindow(pwgt), m_bIconSwitcher(false)
{

    setWindowTitle("System Tray");

    QAction* pactShowHide =
        new QAction("&Показать/Скрыть приложение",this);

    connect(pactShowHide, SIGNAL(triggered()), this,SLOT(slotShowHide()));

    QAction* pactShowMessage = new QAction("Показать уведомление", this);
    connect(pactShowMessage, SIGNAL(triggered()), this, SLOT(slotShowMessage()));

    QAction* pactChangeIcon = new QAction("&Выбрать иконку", this);
    connect(pactChangeIcon, SIGNAL(triggered()),this, SLOT(slotChangeIcon()));

    QAction* pactQuit = new QAction("&Выход", this);
    connect(pactQuit, SIGNAL(triggered()), qApp, SLOT(quit()));

    QAction* pactSleepTime =new QAction("&Сколько осталось спать?)", this);
     connect(pactSleepTime, SIGNAL(triggered()), this, SLOT(slotSleepTime()));

    m_ptrayIconMenu = new QMenu(this);
    m_ptrayIconMenu->addAction(pactShowHide);
    m_ptrayIconMenu->addAction(pactShowMessage);
    m_ptrayIconMenu->addAction(pactChangeIcon);
    m_ptrayIconMenu->addAction(pactQuit);
    m_ptrayIconMenu->addAction(pactSleepTime);

    m_ptrayIcon = new QSystemTrayIcon(this);
    m_ptrayIcon->setContextMenu(m_ptrayIconMenu);
    m_ptrayIcon->setToolTip("SAC");

    slotChangeIcon();

    m_ptrayIcon->show();
}

// ----------------------------------------------------------------------
/*virtual*/void SystemTray::closeEvent(QCloseEvent*)
{
    if (m_ptrayIcon->isVisible())
    {
        hide();
    }
}

// ----------------------------------------------------------------------
void SystemTray::slotShowHide()
{
    parentWidget()->setVisible(!parentWidget()->isVisible());
}


// ----------------------------------------------------------------------
void SystemTray::slotShowMessage()
{
    m_ptrayIcon->showMessage("Информация",
                             "это прога "
                             "крутая!",
                             QSystemTrayIcon::Information,
                             3000);
}

// ----------------------------------------------------------------------
void SystemTray::slotChangeIcon()
{
    m_bIconSwitcher = !m_bIconSwitcher;
    QString strPixmapName = m_bIconSwitcher ? ":/Tray/info.png"
                                            : ":/Tray/clock.png";
    m_ptrayIcon->setIcon(QPixmap(strPixmapName));

}

void SystemTray::slotSleepTime()
{
    m_ptrayIcon->showMessage("Информация", "тебе осталось спать", QSystemTrayIcon::Information,3000);
}
