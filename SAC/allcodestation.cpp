#include "allcodestation.h"

QString allCodeStation::getNameStation() const
{
    return nameStation;
}

void allCodeStation::setNameStation(const QString &value)
{
    nameStation = value;
}

QString allCodeStation::getCodeStation() const
{
    return codeStation;
}

void allCodeStation::setCodeStation(const QString &value)
{
    codeStation = value;
}

QString allCodeStation::getNameRegion() const
{
    return nameRegion;
}

void allCodeStation::setNameRegion(const QString &value)
{
    nameRegion = value;
}

QString allCodeStation::getCodeRegion() const
{
    return codeRegion;
}

void allCodeStation::setCodeRegion(const QString &value)
{
    codeRegion = value;
}

QString allCodeStation::getTypeTransport() const
{
    return typeTransport;
}

void allCodeStation::setTypeTransport(const QString &value)
{
    typeTransport = value;
}

QString allCodeStation::getTypeStation() const
{
    return typeStation;
}

void allCodeStation::setTypeStation(const QString &value)
{
    typeStation = value;
}

QString allCodeStation::getNameCity() const
{
    return nameCity;
}

void allCodeStation::setNameCity(const QString &value)
{
    nameCity = value;
}

QString allCodeStation::getCodeCity() const
{
    return codeCity;
}

void allCodeStation::setCodeCity(const QString &value)
{
    codeCity = value;
}

void allCodeStation::clear()
{
    this->codeCity = "";
    this->nameCity = "";
    this->nameRegion = "";
    this->nameStation = "";
    this->typeStation = "";
    this->typeTransport = "";
    this->codeStation = "";
}

allCodeStation::allCodeStation()
{

}

allCodeStation::allCodeStation(QString nameRegion, QString codeRegion, QString nameCity,
                               QString codeCity, QString nameStation, QString codeStation,
                               QString typeTransport, QString typeStation)
{
    this->codeCity = codeCity;
    this->nameCity = nameCity;
    this->nameRegion = nameRegion;
    this->nameStation = nameStation;
    this->typeStation = typeStation;
    this->typeTransport = typeTransport;
    this->codeStation = codeStation;
}

