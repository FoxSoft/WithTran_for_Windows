#ifndef DATEBASE_H
#define DATEBASE_H

#include <QObject>
#include <QSqlDatabase>
#include <qDebug>
#include <QSqlQuery>
#include <QStringList>
#include <QSql>
#include <QList>
#include <QSqlRecord>

class Datebase : public QObject
{
    Q_OBJECT
    QSqlDatabase* db;
public:
    explicit Datebase(QObject *parent = nullptr);
    bool resetTableCode();
    bool createConnection(const QString&);
    bool createTable(const QString&);
    bool deleteTable(const QString&);
    bool hasTable(const QString&);
    bool isEmpty(const QString& nameTable);
    QStringList nameTables();
    void showNameTableQDebug();
    bool insert(const QString& nameTable, const QString& number, const QString& fromStation,
                    const QString& toStation, const QString& fromTime, const QString& toTimeconst,
                    const QString& duration, const QString& days, const QString& transportType,
                    const QString& uid, const QString& nameReys);

    bool insert(const QString& station, const QString& code);
    QList<QStringList> recordsTable(const QString&);
    QList<QStringList> recordsTableCode();

    QString findCode(const QString&);
    void setCodeAllStation(const QString& nameRegion, const QString& codeRegion,const QString& nameCity, const QString& codeCity,
                           const QString& nameStation, const QString& codeStation,const QString& typeStation, const QString& typeTransport);
    QMap<QString, QString> getListObl();
    QString getNameRegion(QString codeRegion);
    QList<QString> getObl(QString nameRegion);

    QList<QString> searchStation(QString nameStation);
signals:

public slots:
};

#endif // DATEBASE_H
