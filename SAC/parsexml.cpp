#include "parsexml.h"
#include "allcodestation.h"

#include <QFile>
#include <QMessageBox>
#include <QDomNode>
#include <QDebug>

ParseXml::ParseXml(QObject *parent) : QObject(parent)
{

}

void ParseXml::parseXmlRasp()
{

}

void ParseXml::ParseAllCodeStation()
{
    QList<allCodeStation> listCode;
    allCodeStation allCode;
    QDomDocument doc;
    QDomNodeList parent;
    QStringList Lregion, LregionCode;
    QFile file("rasp.xml");
    QString nameStation, codeStation, nameRegion, codeRegion, typeTransport, typeStation, nameCity, codeCity;
    if(file.open(QFile::ReadOnly | QFile::Text))
    {
         doc.setContent(file.readAll());
    }
    file.close();
    parent = doc.elementsByTagName("response");

    if(!parent.isEmpty())
    {
        allCode.clear();
        qDebug() << "non empty";
        QDomNodeList list = parent.at(0).childNodes();
        for(int i = 0; i < list.size(); i++)
        {
            QDomElement elem = list.at(i).toElement();
            if(elem.tagName() == "country")
            {
                 QDomNodeList countryList = elem.childNodes();
                 for (int i = 0; i < countryList.size(); ++i)
                 {
                     QDomElement country = countryList.at(i).toElement();
                     if(country.tagName() == "region")
                     {
                        qDebug() << "\n\n===================регион========================\n\n";

                         QDomNodeList regionList = country.childNodes();
                         for(int j = 0; j < regionList.size(); j++)
                         {
                             QDomElement region = regionList.at(j).toElement();
                             if(region.tagName() == "title")
                             {
                                 Lregion.append(region.text());
                                 nameRegion = region.text();
                                 allCode.setNameRegion(region.text());
                                // qDebug() << "название региона " << region.text();
                             }
                             if(region.tagName() == "codes")
                             {
                                 LregionCode.append(region.text());
                                 codeRegion = region.text();
                                 allCode.setCodeRegion(region.text());
                                 //qDebug() << "код региона " << region.text();
                             }

                             if(region.tagName() == "settlement")
                             {
                                // qDebug() << "--------------------пункты-------------------------";
                                 QDomNodeList settlementList = region.childNodes();
                                 for(int j = 0; j < settlementList.size(); j++)
                                 {
                                     QDomElement title = settlementList.at(j).toElement();
                                     if(title.tagName() == "title")
                                     {
                                        nameCity = title.text();
                                        allCode.setNameCity(title.text());
                                        // qDebug() << "назване города " << title.text();

                                     }
                                     if(title.tagName() == "codes")
                                     {
                                         codeCity = title.text();
                                         allCode.setCodeCity(title.text());
                                        // qDebug() << "код города " << title.text();
                                     }
                                     if(title.tagName() == "station")
                                     {
//                                         qDebug() << nameRegion << codeRegion
//                                                    << nameCity << codeCity
//                                                    << nameStation << codeStation
//                                                    << typeTransport << typeStation;
                                         //qDebug() << "\n-----------станция-----------";
                                         QDomNodeList stationList = title.childNodes();
                                         for(int j = 0; j < stationList.size(); j++)
                                         {
                                             QDomElement titleStation = stationList.at(j).toElement();
                                             if(titleStation.tagName() == "title")
                                             {
                                                 nameStation = titleStation.text();
                                                 allCode.setNameStation(titleStation.text());
                                                // qDebug() << "название станции " << titleStation.text();
                                             }
                                             if(titleStation.tagName() == "transport_type")
                                             {
                                                 typeTransport = titleStation.text();
                                                 allCode.setTypeTransport(titleStation.text());
                                                 //qDebug() << "тип транспорта " << titleStation.text();
                                             }
                                             if(titleStation.tagName() == "station_type")
                                             {
                                                 typeStation = titleStation.text();
                                                 allCode.setTypeStation(titleStation.text());
                                                 //qDebug() << "тип станции "  << titleStation.text();
                                             }
                                             if(titleStation.tagName() == "codes")
                                             {
                                                 codeStation = titleStation.text();
                                                 allCode.setCodeStation(titleStation.text());
                                                 //qDebug() << "код станции "  << titleStation.text();

//                                                 db->setCodeAllStation(allCode.getNameRegion(), allCode.getCodeRegion(),
//                                                                       allCode.getNameCity(), allCode.getCodeCity(),
//                                                                       allCode.getNameStation(), allCode.getCodeStation(),
//                                                                       allCode.getTypeStation(), allCode.getTypeTransport());

                                                 //listCode.append(allCode);
                                                 //allCode.clear();
                                                 db->setCodeAllStation(nameRegion, codeRegion,
                                                                       nameCity, codeCity,
                                                                       nameStation, codeStation,
                                                                       typeStation, typeTransport);

                                                 qDebug() << nameRegion << codeRegion
                                                            << nameCity << codeCity
                                                            << nameStation << codeStation
                                                            << typeTransport << typeStation;

                                             }


                                         }

                                     }

                                 }
                             }

//                             qDebug() << nameRegion << codeRegion
//                                        << nameCity << codeCity
//                                        << nameStation << codeStation
//                                        << typeTransport << typeStation;
                         }
                     }
                 }
            }

        }
    }

qDebug() << "count           " << listCode.count();
}

QList<QStringList> ParseXml::parseTag()
{
       QList<QStringList>  outList;
       QDomDocument doc;
       QDomNodeList parent;
       QFile file("rasp.xml");
       if(file.open(QFile::ReadOnly | QFile::Text))
       {
            doc.setContent(file.readAll());
       }
       file.close();
       //названия городов из поика юзера
       QString LfromTitle, LtoTitle;

       QStringList LthreadsDays, LthreadsArrival, LthreadsDuration, LthreadsDeparture;
       QStringList LthreadsToTypeTransp, Ltotitle, LtoCode, Lfromtitle, LfromCode;
       QStringList LthreadTitle, LthreadNumber, LthreadUid;
       parent = doc.elementsByTagName("response");

               if (!parent.isEmpty())
               {
                   QDomNodeList list = parent.at(0).childNodes();
                   for (int i = 0; i < list.size(); ++i)
                   {
                       QDomElement elem = list.at(i).toElement();

                       if(elem.tagName() == "search")
                       {
                           QDomNodeList searchList = elem.childNodes();
                               for (int i = 0; i < searchList.size(); ++i)
                               {
                                   QDomElement search = searchList.at(i).toElement();

                                        if (search.tagName() == "from")
                                        {
                                            QDomNodeList fromsList = search.childNodes();
                                            for (int j = 0; j < fromsList.size(); ++j)
                                            {
                                                  QDomElement from = fromsList.at(j).toElement();
                                                  if (from.tagName() == "title")
                                                  {
                                                    qDebug() << from.text();
                                                    LfromTitle = from.text();
                                                  }
                                             }
                                         }

                                        if (search.tagName() == "to")
                                        {
                                            QDomNodeList toList = search.childNodes();
                                            for (int j = 0; j < toList.size(); ++j)
                                            {
                                                 QDomElement to = toList.at(j).toElement();
                                                 if (to.tagName() == "title")
                                                 {
                                                     qDebug() << to.text();
                                                    LtoTitle = to.text();
                                                 }
                                            }
                                        }
                                    }
                       }
                       if (elem.tagName() == "threads")
                       {
                                   QDomNodeList threadsList = elem.childNodes();
                                   for (int j = 0; j < threadsList.size(); ++j)
                                   {
                                       QDomElement threads = threadsList.at(j).toElement();
                                      if (threads.tagName() == "days")
                                       {
                                           LthreadsDays << threads.text();
                                           qDebug() << "days " << threads.text();
                                       }
                                      if (threads.tagName() == "arrival")
                                      {
                                          LthreadsArrival << threads.text();
                                          qDebug() << "arrival " << threads.text();
                                      }
                                      if (threads.tagName() == "duration")
                                      {
                                          LthreadsDuration << threads.text();
                                          qDebug() << "duration " << threads.text();
                                      }
                                      if (threads.tagName() == "departure")
                                      {
                                          LthreadsDeparture << threads.text();
                                          qDebug() << "departure " << threads.text();
                                      }
                                      if(threads.tagName() == "to")
                                      {
                                          QDomNodeList threadsToList = threads.childNodes();
                                          for (int j = 0; j < threadsToList.size(); ++j)
                                          {
                                              QDomElement to = threadsToList.at(j).toElement();
                                              if(to.tagName() == "transport_type")
                                              {
                                                  LthreadsToTypeTransp << to.text();
                                                  qDebug() << "transport_type " << to.text();
                                              }
                                          }
                                      }


                                            if (threads.tagName() == "thread")
                                            {
                                                qDebug() << "thread";
                                                QDomNodeList threadList = threads.childNodes();
                                                for (int j = 0; j < threadList.size(); ++j)
                                                {
                                                    QDomElement thread = threadList.at(j).toElement();
                                                  if (thread.tagName() == "title")
                                                  {
                                                     LthreadTitle << thread.text();
                                                     qDebug() << "title " << thread.text();
                                                  }
                                                  if (thread.tagName() == "number")
                                                  {
                                                     LthreadNumber<< thread.text();
                                                     qDebug() << "number " << thread.text();
                                                  }
                                                  if (thread.tagName() == "uid")
                                                  {
                                                      LthreadUid << thread.text();
                                                      qDebug() << "uid " << thread.text();
                                                  }
                                                }
                                             }

                                            if (threads.tagName() == "from")
                                            {
                                                qDebug() << "from";
                                                QDomNodeList fromList = threads.childNodes();
                                                for (int j = 0; j < fromList.size(); ++j)
                                                {
                                                 QDomElement from = fromList.at(j).toElement();
                                                  if (from.tagName() == "title")
                                                  {
                                                    Lfromtitle << from.text();
                                                    qDebug() << "title " << from.text();
                                                  }
                                                 if (from.tagName() == "code")
                                                 {
                                                    LfromCode << from.text();
                                                    qDebug() << "code " << from.text();
                                                 }
                                                }
                                             }

                                            if (threads.tagName() == "to")
                                            {
                                                qDebug() << "to";
                                                QDomNodeList toList = threads.childNodes();
                                                for (int j = 0; j < toList.size(); ++j)
                                                {
                                                 QDomElement to = toList.at(j).toElement();
                                                  if (to.tagName() == "title")
                                                  {
                                                    Ltotitle << to.text();
                                                    qDebug() << "title " << to.text();
                                                  }
                                                 if (to.tagName() == "code")
                                                 {
                                                    LtoCode<< to.text();
                                                    qDebug() << "code " << to.text();
                                                 }
                                                }
                                            }
                                   }
                       qDebug() << "\n\n";
                       }
                   }
                   outList << LthreadsDays <<LthreadsArrival << LthreadsDuration << LthreadsDeparture << LthreadsToTypeTransp
                           << LthreadUid << LthreadTitle << LthreadNumber << Lfromtitle << LfromCode << Ltotitle<< LtoCode;
               }

 /*          заполняем поля класса parseXML
           int item=0;
           QStringList list;
          ListTime.clear();
          duration.clear();
           while (item < outList.at(0).count())
           {
              // время прибытия-отправки
               list << outList.at(0).at(item) << outList.at(0).at(item+2);
               ListTime.insert(item, list);
               //время в пути
               duration << (QString)outList.at(0).at(item+1);
               list.clear();
               item+=3;
           }

           int item2=0;
           int item3=0;
           list.clear();
           while (item2 < outList.at(2).count())
           {
               //codCity = назване города, код
               list << outList.at(2).at(item2+1) << outList.at(2).at(item2);
               codCity.append(list);
               list.clear();
               list << outList.at(3).at(item2+1) << outList.at(3).at(item2);
               codCity.append(list);

               NameStationFrom << outList.at(2).at(item2+1);
               NameStationTo << outList.at(3).at(item2+1);
               //название рейса
               NameReys << outList.at(1).at(item3+1);
               //uid рейса
               UID << outList.at(1).at(item3);
               //номер рейса
               number << outList.at(1).at(item3+2);

               item2+=2;
               item3+=3;
           }


           db = new Datebase();

           if (!db->createConnection("list_station.db"))
           {
              QMessageBox::critical(NULL,QObject::tr("Ошибка"),"Нет подключения к БД 8(");
           }
           else
           {
               db->createTable(LfromTitle+"_"+LtoTitle);
               int i = 0;
               while (i < number.count())
               {
                   db->insert(LfromTitle+"_"+LtoTitle, number.at(i), NameStationFrom.at(i),
                              NameStationTo.at(i), ListTime.at(i).at(1), ListTime.at(i).at(0), duration.at(i), );
                   i++;
               }
           }


           qDebug() << outList.at(0).count();
           qDebug() << outList.at(0);

           qDebug() << outList.at(1).count();
           qDebug() << outList.at(1);

           qDebug() << outList.at(2).count();
           qDebug() << outList.at(2);

           qDebug() << outList.at(3).count();
           qDebug() << outList.at(3);*/


               db = new Datebase();

               if (!db->createConnection("list_station.db"))
               {
                  QMessageBox::critical(NULL,QObject::tr("Ошибка"),"Нет подключения к БД 8(");
               }
               else
               {
                   db->deleteTable(LfromTitle+"_"+LtoTitle);
                   db->createTable(LfromTitle+"_"+LtoTitle);
                   int i = 0;
                   while (i < outList.at(7).count())
                   {
                       db->insert(LfromTitle+"_"+LtoTitle, outList.at(7).at(i),outList.at(8).at(i), outList.at(10).at(i),
                                  outList.at(3).at(i), outList.at(1).at(i), outList.at(2).at(i), outList.at(0).at(i),
                                  outList.at(4).at(i), outList.at(5).at(i), outList.at(6).at(i));
                       i++;
                   }
               }

           /*qDebug() << LfromTitle;
           qDebug() << LtoTitle;
           qDebug() << "\n" << outList.at(0) ;
           qDebug() << "\n" << outList.at(1) ;
           qDebug() << "\n" << outList.at(2) ;
           qDebug() << "\n" << outList.at(3) ;
           qDebug() << "\n" << outList.at(4) ;
           qDebug() << "\n" << outList.at(5) ;
           qDebug() << "\n" << outList.at(6) ;
           qDebug() << "\n" << outList.at(7) ;
           qDebug() << "\n" << outList.at(8) ;
           qDebug() << "\n" << outList.at(9);
           qDebug() << "\n" << outList.at(10);
           qDebug() << "\n" << outList.at(11);*/

           emit recordToDBComplete();
           return outList;

}

QList<QStringList> ParseXml::parseListThread(QString tag)
{
    QFile file("rasp.xml");
    QDomDocument doc;
    QList<QStringList>  outList;
    if(file.open(QFile::ReadOnly | QFile::Text))
    {
        doc.setContent(file.readAll());
        file.close();
    } else outList;

    QDomNodeList parent;
    parent = doc.elementsByTagName(tag);

               if (!parent.isEmpty())
               {
               QStringList Lfrom, Lto;
               QDomNodeList list = parent.at(0).childNodes();
                   for (int i = 0; i < list.size(); ++i)
                   {
                       QDomElement response = list.at(i).toElement();

                            if (response.tagName() == "uid")
                                qDebug() << "uid = " << response.text();

                            if (response.tagName() == "days")
                                qDebug() << "days = " << response.text();

                            if (response.tagName() == "title")
                                qDebug() << "title = " << response.text();

                            if (response.tagName() == "number")
                                qDebug() << "number = " << response.text();

                            if (response.tagName() == "stops")
                            {
                                QDomNodeList stopsList = response.childNodes();
                                for (int j = 0; j < stopsList.size(); ++j)
                                {
                                    QDomElement stops = stopsList.at(j).toElement();
                                    if (stops.tagName() == "duration")
                                        qDebug() << "duration = " << stops.text();

                                    if (stops.tagName() == "departure")
                                        qDebug() << "departure = " << stops.text();

                                    if (stops.tagName() == "stop_time")
                                        qDebug() << "stop_time = " << stops.text();

                                    QDomNodeList stationList = stops.childNodes();
                                    for (int j = 0; j < stationList.size(); ++j)
                                    {
                                        QDomElement station = stationList.at(j).toElement();
                                        if (station.tagName() == "code")
                                            qDebug() << "code = " << station.text();

                                        if (station.tagName() == "title")
                                            qDebug() << "title = " << station.text();

                                        if (station.tagName() == "station_type")
                                            qDebug() << "station_type = " << station.text();
                                    }
                                }
                            }

                   }
               }

                   // outList << Lfrom << Lto;
               return outList;
}

QList<QStringList> ParseXml::parseSchedule(QString tag)
{
    QFile file("rasp.xml");
    QDomDocument doc;
    QList<QStringList>  outList;
    if(file.open(QFile::ReadOnly | QFile::Text))
    {
        doc.setContent(file.readAll());
        file.close();
    } else outList;

    QDomNodeList parent;
    parent = doc.elementsByTagName(tag);

            if (!parent.isEmpty())
            {
            QStringList Lfrom, Lto;
            QDomNodeList list = parent.at(0).childNodes();
                for (int i = 0; i < list.size(); ++i)
                {
                    QDomElement response = list.at(i).toElement();

                         if (response.tagName() == "station")
                         {
                             QDomNodeList stationList = response.childNodes();
                             for (int j = 0; j < stationList.size(); ++j)
                             {
                                 QDomElement station = stationList.at(j).toElement();
                                 if (station.tagName() == "code")
                                     qDebug() << "code = " << station.text();

                                 if (station.tagName() == "title")
                                     qDebug() << "title = " << station.text();
                             }
                         }

                         if (response.tagName() == "schedule")
                         {
                            QDomNodeList scheduleList = response.childNodes();
                            for (int j = 0; j < scheduleList.size(); ++j)
                            {
                                QDomElement schedule = scheduleList.at(j).toElement();
                                 if (schedule.tagName() == "days")
                                     qDebug() << "days = " << schedule.text();

                                 if (schedule.tagName() == "arrival_time")
                                     qDebug() << "arrival_time = " << schedule.text();

                                 if (schedule.tagName() == "departure_time")
                                     qDebug() << "departure_time = " << schedule.text();

                                 if (schedule.tagName() == "thread")
                                 {
                                     QDomNodeList threadList = schedule.childNodes();
                                     for (int j = 0; j < threadList.size(); ++j)
                                     {
                                         QDomElement thread = threadList.at(j).toElement();
                                         if (thread.tagName() == "uid")
                                             qDebug() << "uid = " << thread.text();

                                         if (thread.tagName() == "title")
                                             qDebug() << "title = " << thread.text();

                                         if (thread.tagName() == "number")
                                             qDebug() << "number = " << thread.text();

                                         if (thread.tagName() == "short_title")
                                             qDebug() << "short_title = " << thread.text();

                                         if (thread.tagName() == "code")
                                             qDebug() << "code = " << thread.text();
                                     }
                                 }
                            }
                         }

                         if (response.tagName() == "pagination")
                         {
                             QDomNodeList paginationList = response.childNodes();
                             for (int j = 0; j < paginationList.size(); ++j)
                             {
                                 QDomElement pagination = paginationList.at(j).toElement();
                                 if (pagination.tagName() == "page")
                                     qDebug() << "page = " << pagination.text();

                                 if (pagination.tagName() == "total")
                                     qDebug() << "total = " << pagination.text();

                                 if (pagination.tagName() == "per_page")
                                     qDebug() << "per_page = " << pagination.text();

                                 if (pagination.tagName() == "page_count")
                                     qDebug() << "page_count = " << pagination.text();

                             }
                         }
                }
            }

                // outList << Lfrom << Lto;
            return outList;
}

ParseXml::parseCode()
{
    QFile file("code.xml");
     file.open(QFile::ReadOnly | QFile::Text);
     QDomDocument doc;
     doc.setContent(file.readAll());

     QList<QString> nameCity;
     QList<QString> codCity;

    QDomNodeList list = doc.elementsByTagName("country");
    for(int i=0; i<list.size();i++)
    {
      QDomElement elem = list.at(i).toElement();

            if(elem.attribute("name") == "Россия")
            {
                QDomNodeList listCon = elem.childNodes();
                for(int i=0; i<listCon.size();i++)
                {
                    QDomElement city = listCon.at(i).toElement();
                    if(city.tagName()=="city")
                    {
                        codCity.append("c"+city.attribute("region", "region"));
                        nameCity.append(city.text());
                    }
                }
            }
    }

//    qDebug() << codCity;
//    qDebug() << "\n\n";
//    qDebug() << nameCity;
    db = new Datebase();

    if (!db->createConnection("list_station.db"))
    {
       QMessageBox::critical(NULL,QObject::tr("Ошибка"),"Нет подключения к БД 8(");
    }
    else
    {
        db->resetTableCode();
        for(int i = 0; i < codCity.count(); i++)
        {
            db->insert(nameCity.at(i), codCity.at(i));
        }
    }
}

ParseXml::recordToDB()
{
    qDebug() << parseTag();
}
