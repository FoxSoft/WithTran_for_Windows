import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4


ListView
{
    id:idListView
    width: 400;
    height: 400;
    spacing: 2;
    model: myModel;

    delegate: Rectangle
    {
        height: 120
        width: 350
        color: button_green_mouse_area.pressed ? "#D4D18E" : "#E5E3AA"
        border.color: "#575757"
        border.width: 2
        radius: 5

        Item
        {

            id: itemId
            Image
            {
                id: img
                width: 50;
                height: 50;
                y:30;
                x:6;
                source: statusBusBool ? "/rasptrans/buses_gren.svg" : "/rasptrans/buses_red.svg"
            }
            Label
            {
                id: idNameReys
                text: "<font color='blue'>Маршрут: </font>" + NameReys  // <span style=" font-size:11pt; font-weight:600; color:#ff0000;">S</span>
                anchors.top: parent.top
                anchors.left: parent.Right
                anchors.bottom: parent.bottom
                anchors.margins: 5;
                x:6;
                y:0;
            }
            Label
            {
                id: sfrom
                text: "<font color='red'>Откуда: </font>" + NameStationFrom
                anchors.top: parent.top
                anchors.left: parent.Right
                anchors.bottom: parent.bottom
                anchors.margins: 25;
                x:80;
                y:30;
            }
            Label
            {
                id: sto
                text: "<font color='green'>Куда: </font>" + NameStationTo
                anchors.top: parent.top
                anchors.left: parent.Right
                anchors.bottom: parent.bottom
                anchors.margins: 40;
                x:80;
                y:30;
            }

            Label
            {
                id: tfrom
                text: "<font color='red'>Отправка: </font>" + TimeFrom
                anchors.top: parent.top
                anchors.left: parent.Righ
                anchors.bottom: parent.bottom
                anchors.margins: 60;
                x:80;
                y:30;
            }

            Label
            {
                id: tto
                text: "<font color='green'>Прибытие: </font>" + TimeTo
                anchors.top: parent.top
                anchors.left: parent.Righ
                anchors.bottom: parent.bottom
                anchors.margins: 75;
                x:80;
                y:30;
            }

            Label
            {
                id: idStatus
                text: "<font color='blue'>Статус: </font>" + Status
                anchors.top: parent.top
                anchors.left: parent.Righ
                anchors.right: parent.Righ
                anchors.bottom: parent.bottom
                anchors.margins: 60;
                x:180;
                y:30;
            }

            Label
            {
                id: idDur
                text: "<font color='blue'>В пути: </font>" + Duration
                anchors.top: parent.top
                anchors.left: parent.Righ
                anchors.right: parent.Righ
                anchors.bottom: parent.bottom
                anchors.margins: 75;
                x:180;
                y:30;
            }

            Label
            {
                id: idNum
                text: "<font color='blue'>№: </font>" + Numbers
                x:6;
                y:70;
            }

            Label
            {
                x:6;
                y:90;
                id: idDays
                text: Days


                }

            Loader { id: pageLoader }
        }

        MouseArea {
                    id: button_green_mouse_area
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered:
                    {
                        parent.border.color = "black"
                    }
                    onExited:
                    {
                        parent.border.color = "#575757"
                    }
                    onClicked:
                    {
                       // statusBusBool ? print("не ушел") : print("ушел")

                    }
                }

    }

}


