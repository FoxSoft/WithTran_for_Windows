#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "downloaderxml.h"
#include "parsexml.h"
#include "downloader.h"
#include "message.h"
#include "alarmclock.h"
#include "systemtray.h"
#include "obj.h"

#include <QListWidget>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    //
    SystemTray* Tray;
    Datebase *db;

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    //для таскания формы
    void mouseMoveEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    //загрузка вариатов в комбобоксы
    //запихивает данные в лист
    void loadVarintSearch();
    //запись вариантов в файлы
    void writeVarintSearch();
    //блок кнопок
    void blockButton();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_btn_clicked();

    void on_pushButton_5_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

    void on_checkBox_clicked();

    void on_statusAlarmClock_clicked();

    void on_pushButtonParsingCode_clicked();

    void on_pushButton_6_clicked();

private:
    //координаты курсора
    QPoint myPos;

    DownloaderXml *down;
    //загрузчик файлов из интернета
    Downloader* downloader;
    //
    Message* message;
    //
    ParseXml* parsexml;
    //
    AlarmClock* alarmclock;
    //
    obj *o;

private:
    //для отгрузки из ComoBoxFrom
    QString searchFrom;
    //для отгрузки из ComoBoxTo
    QString searchTo;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
