#include "downloader.h"
#include <QDebug>
#include <QMessageBox>

Downloader::Downloader(QObject *parent) : QObject(parent)
{
    // Создаем объект менеджера
        manager = new QNetworkAccessManager(this);
//               // ... и подключаем сигнал о завершении получения данных к обработчику полученного ответа
//               connect(manager, &QNetworkAccessManager::finished, this, &Downloader::replyFinished);
}

void Downloader::getExcel(QString Url, QString NameFile)
{
    Download(Url+NameFile, 1);
}

void Downloader::getXMLRaspFromTo(QString From, QString To)
{

    Download("https://api.rasp.yandex.net/v1.0/search/"
                     "?apikey=86973449-b547-417e-95da-e70f87ef8748&format="
             "xml&from="+From+"&to="+To+"&lang=ru&page=1", 0);
}

void Downloader::getXMLSchedule(QString Station)
{
    Download("https://api.rasp.yandex.net/v1.0/schedule/?apikey=86973449-b547-417e-95da-e70f87ef8748"
             "&format=xml&station="+Station+"&lang=ru&transport_types=bus",2);
}

void Downloader::getXMLListThread(QString uid)
{
    Download("https://api.rasp.yandex.net/v1.0/thread/?apikey=86973449-b547-417e-95da-e70f87ef8748"
             "&format=xml&uid="+uid+"&lang=ru&show_systems=all",3);
}

void Downloader::getAllCodeStation()
{
    Download("https://api.rasp.yandex.net/v3.0/stations_list/"
             "?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml",4);
}


void Downloader::Download(QString Link, int mode)
{
    // берем адрес введенный в текстовое поле
    QUrl url(Link);
    // создаем объект для запроса
    QNetworkRequest request(url);

    // Выполняем запрос, получаем указатель на объект
    // ответственный за ответ
    QNetworkReply* reply=  manager->get(request);

    // Подписываемся на сигнал о готовности загрузки
    connect( reply, SIGNAL(finished()),
             this, SLOT(replyFinished()) );

}

void Downloader::replyFinished()
{
    QNetworkReply *reply=
      qobject_cast<QNetworkReply *>(sender());

    if (reply->error() == QNetworkReply::NoError)
    {
      if(reply->hasRawHeader("Location"))
      {
          // Запрос по перенаправлению
          QString locationUrl= reply->header(QNetworkRequest::LocationHeader).toString();
          QNetworkRequest request(locationUrl);
          QNetworkReply* reply=  manager->get(request);
          connect( reply, SIGNAL(finished()),
                   this, SLOT(replyFinished()) );
      }
      else
      {
          if(check == 0)//обычное расс=писание от А до В
          {
            QFile file("rasp.xml");
            if( file.open(QIODevice::WriteOnly) )
            {
              QByteArray content= reply->readAll();
              file.write(content); // пишем в файл
            }
             nameFile = false;
          }
          if(check == 1)//расписание занятий
          {
              QFile file("raspStud.xls");
              if( file.open(QIODevice::WriteOnly) )
              {
                QByteArray content= reply->readAll();
                file.write(content); // пишем в файл
              }
          }
          if(check == 2)//расписание рейсов по станции
          {
              QFile file("raspFromStation.xml");
              if( file.open(QIODevice::WriteOnly) )
              {
                QByteArray content= reply->readAll();
                file.write(content); // пишем в файл
              }
          }
          if(check == 3)//список станции по рейсу
          {
              QFile file("listStation.xml");
              if( file.open(QIODevice::WriteOnly) )
              {
                QByteArray content= reply->readAll();
                file.write(content); // пишем в файл
              }
          }
          if(check == 4)//список станции по рейсу
          {
              QFile file("listCodeStation.xml");
              if( file.open(QIODevice::WriteOnly) )
              {
                QByteArray content= reply->readAll();
                file.write(content); // пишем в файл
              }
          }
         emit DownloadComplete();
    }
    }
    else
    {
        // вывод ошибки
        qDebug() << reply->errorString();
        emit error();
        QMessageBox::information(0,"Статус загрузки", reply->errorString(), QMessageBox::Cancel);

    }

    reply->deleteLater();
}
