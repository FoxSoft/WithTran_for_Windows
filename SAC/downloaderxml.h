#ifndef DOWNLOADERXML_H
#define DOWNLOADERXML_H


#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QUrl>
#include <QDebug>
#include <QXmlStreamReader>
#include <QDomNode>

class DownloaderXml : public QObject
{
    Q_OBJECT
public:

    void parseXmlRasp();
    void loadRaspFromFile();
    void traverseNode(const QDomNode& node);
    explicit DownloaderXml(QObject *parent = 0);
    QString getNode2 (QString sTag, QDomElement element);
    QString getNode(QString sTag, QDomElement element);
    QString LoadFilleWindow(QString patch);
    QList<QStringList> parseTag(QDomNodeList &parent, QDomDocument &doc, QString tag);
    QString setCodeCity(QString From, QString To);
    QString getAllCodeStation();

signals:

    void onReady();


public slots:

    void getData();     // Метод инициализации запроса на получение данных
    void onResult(QNetworkReply *reply);    // Слот обработки ответа о полученных данных
    void readFile();
    //слот
    void readFileXml();

private:

    QNetworkAccessManager *manager; // менеджер сетевого доступа
    QXmlStreamReader xmlReader;
    DownloaderXml *downloader;

public:
    //данные xml
    QString XmlData;
    //время отправления
    QMap<QString,QString> ListTime;
    //имя ст. отправки
    QString NameStationFrom;
    //имя ст. прибытия
    QString NameStationTo;
    //название рейса
    QString NameReys;
    //время в пути
    QList<QString> duration;
    //номер рейса
    QString number;
    //код города по ключу названия города
    QMap<QString, QString> codCity;

};

#endif // DOWNLOADERXML_H
