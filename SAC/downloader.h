﻿#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QFile>
#include <QNetworkAccessManager>
#include <QMessageBox>
#include <QtNetwork/QNetworkReply>
#include <QUrl>

class Downloader : public QObject
{
    Q_OBJECT
public:

    QNetworkAccessManager *manager;
    explicit Downloader(QObject *parent = 0);

public:
    // для excel, этот метод ка вспомогательный для Download(QString Link)
    void getExcel(QString Url, QString NameFile);

    //для getXMLRaspFromTo, этот метод ка вспомогательный для Download(QString Link)
    void getXMLRaspFromTo(QString From, QString To);

    //для getXMLSchedule, этот метод ка вспомогательный для Download(QString Link)
    void getXMLSchedule(QString Station);

    //для getXMLListThread, этот метод ка вспомогательный для Download(QString Link)
    void getXMLListThread(QString uid);

    void getAllCodeStation();

private:
    //главный метод загрузки
    void Download(QString Link, int mode);
    bool nameFile = false;
    int check = 0;

public slots:

    void replyFinished();

signals:
    // сигнал о завершении загрузки
    void DownloadComplete();
    // сигнал о ошибки загрузки
    void error();
};

#endif // DOWNLOADER_H
