#include "schedulesubjects.h"

ScheduleSubjects::ScheduleSubjects(const QString& str, int x0, int y0, int x, int y)
{
    textEdit = new QTextEdit();
    textEdit->setFontPointSize(12);
    textEdit->setReadOnly(true);
    textEdit->setMinimumSize(QSize(200,300));
    textEdit->setMaximumSize(QSize(250,3000));

    QHBoxLayout* hLayout = new QHBoxLayout();
    tableWgt = new QTableWidget();
    tableWgt->verticalHeader()->hide();
    QStringList list;
    list << "д/н" << "Часы" << "ИВТ-31" << "кб";
    tableWgt->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QAxObject* excel = new QAxObject("Excel.Application", 0);
    QAxObject* workbooks = excel->querySubObject("Workbooks");
    QAxObject* workbook = workbooks->querySubObject("Open(const QString&)", str);
    QAxObject* sheets = workbook->querySubObject("Worksheets");
    QAxObject* sheet = sheets->querySubObject("Item(int)", 1);

    int countRows = y;
    int countCols = x;

    tableWgt->setRowCount(countRows - (y0 - 1));
    tableWgt->setColumnCount(countCols);
    int i(0);
    for ( int row = (y0 - 1); row < countRows; row++ ){
        for (int column = (x0 - 1); column < countCols; column++ ){
            QAxObject* cell = sheet->querySubObject("Cells(int,int)", row + 1, column + 1);
            QVariant value = cell->property("Value");
            QTableWidgetItem* item = new QTableWidgetItem(value.toString());
            tableWgt->setItem(i, column, item);
        }
        i++;
    }
    deleteEmptyCell();
    autoSize();
    workbook->dynamicCall("Close()");
    excel->dynamicCall("Quit()");
    delete excel;
    tableWgt->setHorizontalHeaderLabels(list);

   // hLayout->setAlignment(Qt::AlignTop);
    hLayout->addWidget(tableWgt);
    hLayout->addWidget(textEdit);
    hLayout->setStretch(1,5);
    setLayout(hLayout);
    tableWgt->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

void ScheduleSubjects::deleteEmptyCell()
{
    for ( int row = 0; row < tableWgt->rowCount(); row++ ){
        bool isRowEmpty = true;
        for (int column = 0; column < tableWgt->columnCount(); column++ ){
            if (tableWgt->item(row, column)->text() != "") {
                isRowEmpty = false;
            }
        }
        if (isRowEmpty == true) {
            tableWgt->removeRow(row);
        }
    }
}

void ScheduleSubjects::autoSize()
{
    int i(0);
    int widthAllColumn(0);
    int heightAllRow(0);
    int maxWidthAllColumn(0);
    for (int row = 0; row < tableWgt->rowCount(); row++ )
    {
        tableWgt->item(row, 2)->setText(tableWgt->item(row, 2)->text().insert(tableWgt->item(row, 2)->text().indexOf("/"), '\n'));
        tableWgt->item(row, 2)->setText(tableWgt->item(row, 2)->text().insert(tableWgt->item(row, 2)->text().indexOf(";", 80) + 1, '\n'));
        tableWgt->resizeRowsToContents();
    for (int column = 0; column < tableWgt->columnCount(); column++ )
    {
        widthAllColumn += tableWgt->columnWidth(column);
    }
    if (maxWidthAllColumn < widthAllColumn)
        maxWidthAllColumn = widthAllColumn;
        heightAllRow += tableWgt->rowHeight(i);
        i++;
        widthAllColumn = 0;
    }
     tableWgt->setMaximumHeight(heightAllRow + 30);
     tableWgt->setMaximumWidth(maxWidthAllColumn);
     tableWgt->setMinimumHeight(heightAllRow - 600);
     tableWgt->setMinimumWidth(maxWidthAllColumn * 1.35);

}

QString ScheduleSubjects::timeFirstSubject(const QString& strDate)
{
    textEdit->setText("");
    QString newDayWeek = strDate.mid(0, strDate.indexOf("."));
    QString newDate = strDate.mid(strDate.indexOf(".") + 1);

    QString dayWeek;
    if (newDayWeek == "понедельник") {
        dayWeek = "пон";
    }
    else if (newDayWeek == "вторник") {
        dayWeek = "вт";
    }
    else if (newDayWeek == "среда") {
        dayWeek = "ср";
    }
    else if (newDayWeek == "четверг") {
        dayWeek = "чт";
    }
    else if (newDayWeek == "пятница") {
        dayWeek = "пят";
    }
    else if (newDayWeek == "суббота") {
        dayWeek = "суб";
    }
    else {return "error";}
    int row;
    QString time("");
    QString date("");
    QString typeSubject("");
    QString subject("");
    QString office("");
    QString x("");
    for (row = 0; row < tableWgt->rowCount(); row++ ){
        if (tableWgt->item(row, 0)->text() == dayWeek) {
            while (row < tableWgt->rowCount() && (tableWgt->item(row, 0)->text() == "" || tableWgt->item(row, 0)->text() == dayWeek)) {
                if(tableWgt->item(row, 2)->text() != "") {
                    QString str = tableWgt->item(row, 2)->text();
                    time = tableWgt->item(row, 1)->text().mid(0, 5);
                    subject = str.mid(0, str.indexOf("/") - 1);
                    str = str.remove(0, str.indexOf("/"));
                    office = tableWgt->item(row, 3)->text();
                    QRegExp reg("[0-9]");
                    for (int i(0); i < str.length(); i++) {
                        if (str[i] == '/') {
                            typeSubject = str.mid(i, str.indexOf("/", i + 1) - i + 1);
                            i += typeSubject.length();
                        }
                        else if (reg.indexIn(str, i) == i) {
                            date = str.mid(i, 5);
                            i += 5;
                        }
                        if (date == newDate) {
                            textEdit->append("День недели: " + newDayWeek);
                            textEdit->append("Пара начинается в: " + time);
                            textEdit->append("Предмет: " + subject);
                            textEdit->append("Дата: " + date);
                            textEdit->append("Тип занятий: " + typeSubject);
                            textEdit->append("Кабинет: " + office);
                            QRegExp reg2("[А-Я]+[а-я]");
                            int k = reg2.indexIn(str, i);
                            textEdit->append("Преподаватель: " + str.mid(k, str.length() - k));
                            timeSubject = time;
                            return "";
                            break;
                        }

                    }
                }
                row++;
            }
        }
    }
    return "error";
}

void ScheduleSubjects::setTimeFirstSubject(const QString& date)
{
    timeFirstSubject(date);
}

void ScheduleSubjects::setTimeFirstSubject(const QDate& date)
{
    QString strDate = date.toString("dddd.dd.MM");
    timeFirstSubject(strDate);
}

//void ScheduleSubjects::checkUpdate()
//{
////    QFile file("file.txt");
////    file.open(QIODevice::ReadOnly);
////    QTextStream in(&file);
////    QString myText = in.readAll();
////    QString date = myText.mid(0, 8);
////    file.close();
////    file.open(QIODevice::WriteOnly);
////    QDate dat;
////    dat = QDate::currentDate();
////    bool isDateDifferent = date.mid(3, 5) != dat.toString("dd.dd.MM").mid(3, 5);
////    bool isDayMoreSeven = (dat.toString("dd.dd.MM").mid(0, 2).toInt() - date.mid(0, 2).toInt()) > 7;
////    if (isDateDifferent || isDayMoreSeven) {
////        date = dat.toString("dd.dd.MM");
////        emit readyUpdate();
////    }
////    textEdit->append(date);
////    QTextStream writeStream(&file);
////    writeStream << date;
////    file.close();
//}
