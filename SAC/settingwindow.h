#ifndef SETTINGWINDOW_H
#define SETTINGWINDOW_H

#include <QMainWindow>
#include "datebase.h"
namespace Ui {
class SettingWindow;
}

class SettingWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SettingWindow(QWidget *parent = 0);
    ~SettingWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::SettingWindow *ui;
    Datebase *db;
};

#endif // SETTINGWINDOW_H
