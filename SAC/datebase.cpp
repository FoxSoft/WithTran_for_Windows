#include "datebase.h"
#include <QObject>
#include <QtSql>

Datebase::Datebase(QObject *parent) : QObject(parent)
{

}

bool Datebase::createConnection(const QString& nameDatabase)
{
    QSqlDatabase newdb = QSqlDatabase::addDatabase("QSQLITE");
    db = &newdb;
    db->setDatabaseName(nameDatabase);

    if (!db->open()) {
        qDebug() << "Cannot open database";
        return false;
    }
    return true;
}

bool Datebase::createTable(const QString& nameTable)
{
    QSqlQuery query;
    QString   str  = "CREATE TABLE " + nameTable + " ( "
                         "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                         "number         VARCHAR, "
                         "from_station   VARCHAR, "
                         "to_station     VARCHAR, "
                         "from_time      VARCHAR, "
                         "to_time        VARCHAR, "
                         "duration       VARCHAR, "
                         "days           VARCHAR, "
                         "transportType  VARCHAR, "
                         "uid            VARCHAR, "
                         "name_reys      VARCHAR "
                         ");";

    if (!query.exec(str)) {
        qDebug() << "Unable to create a table";
        return false;
    }
    return true;
}

bool Datebase::resetTableCode()
{
    QSqlQuery query;
    QString   str  = "DROP TABLE station_code;";
    query.exec(str);

    str  = "CREATE TABLE station_code ( "
           "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
           "station   VARCHAR, "
           "code      VARCHAR"
           ");";

    if (!query.exec(str)) {
        qDebug() << "Unable to create a table";
        return false;
    }
    return true;
}

bool Datebase::deleteTable(const QString& nameTable)
{
    QSqlQuery query;
    QString   str  = "DROP TABLE " + nameTable + ";";
    if (!query.exec(str)) {
        qDebug() << "Unable to delete a table";
        return false;
    }
    return true;
}

bool Datebase::hasTable(const QString& nameTable)
{
    QStringList list = db->tables ();
    foreach (QString str, list) {
        if(str == nameTable) {
            return true;
        }
    }
    return false;
}

QStringList Datebase::nameTables()
{
    QStringList nameTable;
    QStringList list = db->tables ();
    foreach (QString str, list) {
       nameTable.push_back(str);
    }
    return nameTable;
}

void Datebase::showNameTableQDebug(){
    QStringList list = db->tables ();
    foreach (QString str, list) {
       qDebug() << str << " ";
    }
}

bool Datebase::insert(const QString& nameTable, const QString& number, const QString& fromStation,
                      const QString& toStation, const QString& fromTime, const QString& toTime,
                      const QString& duration, const QString& days, const QString& transportType,
                      const QString& uid, const QString& nameReys)
{
    QSqlQuery query;
    QString   str  = "INSERT INTO " + nameTable + " (number, from_station, to_station, from_time, "
                     "to_time, duration, days, transportType, uid, name_reys)"
                     "VALUES('" + number + "', '" + fromStation + "', '" + toStation + "', '" + fromTime +
                     "', '" + toTime + "', '" + duration + "', '" + days + "', '" + transportType +
                     "', '" + uid + "', '" + nameReys +"');";
    if (!query.exec(str)) {
        qDebug() << "Unable to insert a table";
        return false;
    }
    return true;
}

bool Datebase::insert(const QString& station, const QString& code)
{
    QSqlQuery query;
    QString   str  = "INSERT INTO station_code(station, code)"
                     "VALUES('" + station + "', '" + code + "');";
    if (!query.exec(str)) {
        qDebug() << "Unable to insert a table";
        return false;
    }
    return true;
}

void Datebase::setCodeAllStation(const QString& nameRegion, const QString& codeRegion,const QString& nameCity, const QString& codeCity,
                                 const QString& nameStation, const QString& codeStation,const QString& typeStation, const QString& typeTransport)
{
    QSqlQuery query;
    QString   str  = "INSERT INTO codeAllStation(nameRegion, codeRegion, nameCity, codeCity, nameStation,   codeStation"
                     ", typeStation, typeTransport)"
                     "VALUES('" + nameRegion + "', '" + codeRegion + "', '" + nameCity + "', '" + codeCity +
            "', '" + nameStation + "', '" + codeStation +"', '" + typeStation +"', '" + typeTransport +"');";
    if (!query.exec(str)) {
        qDebug() << "Unable to insert a table";

    }

}

QList<QStringList> Datebase::recordsTable(const QString& nameTable)
{
    QList<QStringList> listRecordsTable;

    QStringList number;
    QStringList fromStation;
    QStringList toStation;
    QStringList fromTime;
    QStringList toTime;
    QStringList duration;
    QStringList days;
    QStringList transportType;
    QStringList nameReys;


    QSqlQuery query;
    if (!query.exec("SELECT * FROM " + nameTable + ";")) {
        qDebug() << "Unable to execute query - exiting";
    }
    else {
        QSqlRecord rec = query.record();
        while (query.next()) {
            number.push_back(query.value(rec.indexOf("number")).toString());
            fromStation.push_back(query.value(rec.indexOf("from_station")).toString());
            toStation.push_back(query.value(rec.indexOf("to_station")).toString());
            fromTime.push_back(query.value(rec.indexOf("from_time")).toString());
            toTime.push_back(query.value(rec.indexOf("to_time")).toString());
            duration.push_back(query.value(rec.indexOf("duration")).toString());
            days.push_back(query.value(rec.indexOf("days")).toString());
            transportType.push_back(query.value(rec.indexOf("transportType")).toString());
            nameReys.push_back(query.value(rec.indexOf("name_reys")).toString());

        }
        listRecordsTable.push_back(number);
        listRecordsTable.push_back(fromStation);
        listRecordsTable.push_back(toStation);
        listRecordsTable.push_back(fromTime);
        listRecordsTable.push_back(toTime);
        listRecordsTable.push_back(duration);
        listRecordsTable.push_back(days);
        listRecordsTable.push_back(transportType);
        listRecordsTable.push_back(nameReys);
    }
    return listRecordsTable;
}

QList<QStringList>  Datebase::recordsTableCode()
{
    QList<QStringList> listRecordsTable;
    QStringList station;
    QStringList code;
    QSqlQuery query;
    if (!query.exec("SELECT * FROM station_code;")) {
        qDebug() << "Unable to execute query - exiting";
    }
    else {
        QSqlRecord rec = query.record();
        while (query.next()) {
            station.push_back(query.value(rec.indexOf("station")).toString());
            code.push_back(query.value(rec.indexOf("code")).toString());
        }
        listRecordsTable.push_back(station);
        listRecordsTable.push_back(code);
    }
    return listRecordsTable;
}

QString Datebase::findCode(const QString& town)
{
    QSqlQuery query;
    if (!query.exec("SELECT * FROM station_code WHERE station = '" + town + "';")) {
        return "";
    }
    else
    {
        QSqlRecord rec = query.record();
        while (query.next())
        {
            return query.value(rec.indexOf("code")).toString();
        }
    }
}

bool Datebase::isEmpty(const QString& nameTable)
{
    QSqlQuery query;
    if (!query.exec("SELECT * FROM " + nameTable + ";")) {
        return true;
    }
    else {
        QSqlRecord rec = query.record();
        query.next();
        if (!query.value(rec.indexOf("number")).toBool()) {
            return false;
        }
        else {
            return true;
        }
    }
}

QMap<QString ,QString> Datebase::getListObl()
{
    QList<QStringList> listRecordsTable;
    QStringList nameRegion;
    QStringList codeRegion;
    QMap<QString, QString> mapObl;
    QSqlQuery query;
    if (!query.exec("SELECT * FROM codeAllStation WHERE nameRegion <> '';")) {
        qDebug() << "Unable to execute query - exiting";
    }
    else {
        QSqlRecord rec = query.record();
        while (query.next()) {
            nameRegion.push_back(query.value(rec.indexOf("nameRegion")).toString());
            codeRegion.push_back(query.value(rec.indexOf("codeRegion")).toString());
            mapObl.insert(query.value(rec.indexOf("nameRegion")).toString(), query.value(rec.indexOf("codeRegion")).toString());
        }
        //listRecordsTable.push_back(station);
        //listRecordsTable.push_back(code);
    }
    return mapObl;
}

QString Datebase::getNameRegion(QString codeRegion)
{
    QSqlQuery query;
    QString result;
    if (!query.exec("select nameRegion from codeAllStation"
                    " where codeRegion = '" + codeRegion + "' and nameRegion <> '';")) {
        qDebug() << "Unable to execute query - exiting";
    }
    else {
        QSqlRecord rec = query.record();
        while (query.next()) {
            result = query.value(rec.indexOf("nameRegion")).toString();
        }
        //listRecordsTable.push_back(station);
        //listRecordsTable.push_back(code);
    }
    return result;
}

QList<QString> Datebase::getObl(QString nameRegion)
{
    QSqlQuery query;
    QList<QString> result;
    if (!query.exec("select nameRegion from codeAllStation "
                    "where nameRegion like '" + nameRegion + "%';")) {
        qDebug() << "Unable to execute query - exiting";
    }
    else {
        QSqlRecord rec = query.record();
        while (query.next()) {
            result.append(query.value(rec.indexOf("nameRegion")).toString());
        }
    }
    return result;
}

QList<QString> Datebase::searchStation(QString nameStation)
{
    QSqlQuery query;
    QList<QString> result;
    if(!query.exec("select * from codeAllStation"
                   "where (nameStation like '" + nameStation + "%') or (nameStation like '" + nameStation + "%');"))
    {
        qDebug() << "Unable to execute query - exiting";
    }
    else {
        QSqlRecord rec = query.record();
        while (query.next()) {
           result.append(query.value(rec.indexOf("nameStation")).toString());
        }
    }
    qDebug() << result;
    return result;
}
