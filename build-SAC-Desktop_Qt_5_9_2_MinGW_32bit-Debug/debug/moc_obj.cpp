/****************************************************************************
** Meta object code from reading C++ file 'obj.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../SAC/obj.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'obj.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_obj_t {
    QByteArrayData data[23];
    char stringdata0[286];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_obj_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_obj_t qt_meta_stringdata_obj = {
    {
QT_MOC_LITERAL(0, 0, 3), // "obj"
QT_MOC_LITERAL(1, 4, 22), // "NameStationFromChanged"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 20), // "NameStationToChanged"
QT_MOC_LITERAL(4, 49, 15), // "TimeFromChanged"
QT_MOC_LITERAL(5, 65, 13), // "TimeToChanged"
QT_MOC_LITERAL(6, 79, 13), // "StatusChanged"
QT_MOC_LITERAL(7, 93, 15), // "NameReysChanged"
QT_MOC_LITERAL(8, 109, 15), // "DurationChanged"
QT_MOC_LITERAL(9, 125, 14), // "NumbersChanged"
QT_MOC_LITERAL(10, 140, 11), // "DaysChanged"
QT_MOC_LITERAL(11, 152, 20), // "statusBusBoolChanged"
QT_MOC_LITERAL(12, 173, 14), // "timer_overflow"
QT_MOC_LITERAL(13, 188, 15), // "NameStationFrom"
QT_MOC_LITERAL(14, 204, 13), // "NameStationTo"
QT_MOC_LITERAL(15, 218, 8), // "TimeFrom"
QT_MOC_LITERAL(16, 227, 6), // "TimeTo"
QT_MOC_LITERAL(17, 234, 6), // "Status"
QT_MOC_LITERAL(18, 241, 8), // "NameReys"
QT_MOC_LITERAL(19, 250, 8), // "Duration"
QT_MOC_LITERAL(20, 259, 7), // "Numbers"
QT_MOC_LITERAL(21, 267, 4), // "Days"
QT_MOC_LITERAL(22, 272, 13) // "statusBusBool"

    },
    "obj\0NameStationFromChanged\0\0"
    "NameStationToChanged\0TimeFromChanged\0"
    "TimeToChanged\0StatusChanged\0NameReysChanged\0"
    "DurationChanged\0NumbersChanged\0"
    "DaysChanged\0statusBusBoolChanged\0"
    "timer_overflow\0NameStationFrom\0"
    "NameStationTo\0TimeFrom\0TimeTo\0Status\0"
    "NameReys\0Duration\0Numbers\0Days\0"
    "statusBusBool"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_obj[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
      10,   80, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,
       3,    0,   70,    2, 0x06 /* Public */,
       4,    0,   71,    2, 0x06 /* Public */,
       5,    0,   72,    2, 0x06 /* Public */,
       6,    0,   73,    2, 0x06 /* Public */,
       7,    0,   74,    2, 0x06 /* Public */,
       8,    0,   75,    2, 0x06 /* Public */,
       9,    0,   76,    2, 0x06 /* Public */,
      10,    0,   77,    2, 0x06 /* Public */,
      11,    0,   78,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,   79,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

 // properties: name, type, flags
      13, QMetaType::QString, 0x00495103,
      14, QMetaType::QString, 0x00495103,
      15, QMetaType::QString, 0x00495103,
      16, QMetaType::QString, 0x00495103,
      17, QMetaType::QString, 0x00495103,
      18, QMetaType::QString, 0x00495103,
      19, QMetaType::QString, 0x00495103,
      20, QMetaType::QString, 0x00495103,
      21, QMetaType::QString, 0x00495103,
      22, QMetaType::Bool, 0x00495003,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,

       0        // eod
};

void obj::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        obj *_t = static_cast<obj *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->NameStationFromChanged(); break;
        case 1: _t->NameStationToChanged(); break;
        case 2: _t->TimeFromChanged(); break;
        case 3: _t->TimeToChanged(); break;
        case 4: _t->StatusChanged(); break;
        case 5: _t->NameReysChanged(); break;
        case 6: _t->DurationChanged(); break;
        case 7: _t->NumbersChanged(); break;
        case 8: _t->DaysChanged(); break;
        case 9: _t->statusBusBoolChanged(); break;
        case 10: _t->timer_overflow(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::NameStationFromChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::NameStationToChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::TimeFromChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::TimeToChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::StatusChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::NameReysChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::DurationChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::NumbersChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::DaysChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (obj::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&obj::statusBusBoolChanged)) {
                *result = 9;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        obj *_t = static_cast<obj *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->NameStationFrom(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->NameStationTo(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->TimeFrom(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->TimeTo(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->Status(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->NameReys(); break;
        case 6: *reinterpret_cast< QString*>(_v) = _t->Duration(); break;
        case 7: *reinterpret_cast< QString*>(_v) = _t->Numbers(); break;
        case 8: *reinterpret_cast< QString*>(_v) = _t->Days(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->statusBusBool(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        obj *_t = static_cast<obj *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setNameStationFrom(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setNameStationTo(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setTimeFrom(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setTimeTo(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setStatus(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setNameReys(*reinterpret_cast< QString*>(_v)); break;
        case 6: _t->setDuration(*reinterpret_cast< QString*>(_v)); break;
        case 7: _t->setNumbers(*reinterpret_cast< QString*>(_v)); break;
        case 8: _t->setDays(*reinterpret_cast< QString*>(_v)); break;
        case 9: _t->setstatusBusBool(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject obj::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_obj.data,
      qt_meta_data_obj,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *obj::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *obj::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_obj.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int obj::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void obj::NameStationFromChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void obj::NameStationToChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void obj::TimeFromChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void obj::TimeToChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void obj::StatusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void obj::NameReysChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void obj::DurationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void obj::NumbersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void obj::DaysChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void obj::statusBusBoolChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
