/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../SAC/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[23];
    char stringdata0[409];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 14), // "mouseMoveEvent"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 12), // "QMouseEvent*"
QT_MOC_LITERAL(4, 40, 1), // "e"
QT_MOC_LITERAL(5, 42, 15), // "mousePressEvent"
QT_MOC_LITERAL(6, 58, 17), // "mouseReleaseEvent"
QT_MOC_LITERAL(7, 76, 16), // "loadVarintSearch"
QT_MOC_LITERAL(8, 93, 17), // "writeVarintSearch"
QT_MOC_LITERAL(9, 111, 11), // "blockButton"
QT_MOC_LITERAL(10, 123, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(11, 145, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(12, 169, 14), // "on_btn_clicked"
QT_MOC_LITERAL(13, 184, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(14, 208, 25), // "on_listWidget_itemClicked"
QT_MOC_LITERAL(15, 234, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(16, 251, 4), // "item"
QT_MOC_LITERAL(17, 256, 22), // "on_radioButton_clicked"
QT_MOC_LITERAL(18, 279, 24), // "on_radioButton_2_clicked"
QT_MOC_LITERAL(19, 304, 19), // "on_checkBox_clicked"
QT_MOC_LITERAL(20, 324, 27), // "on_statusAlarmClock_clicked"
QT_MOC_LITERAL(21, 352, 32), // "on_pushButtonParsingCode_clicked"
QT_MOC_LITERAL(22, 385, 23) // "on_pushButton_6_clicked"

    },
    "MainWindow\0mouseMoveEvent\0\0QMouseEvent*\0"
    "e\0mousePressEvent\0mouseReleaseEvent\0"
    "loadVarintSearch\0writeVarintSearch\0"
    "blockButton\0on_pushButton_clicked\0"
    "on_pushButton_4_clicked\0on_btn_clicked\0"
    "on_pushButton_5_clicked\0"
    "on_listWidget_itemClicked\0QListWidgetItem*\0"
    "item\0on_radioButton_clicked\0"
    "on_radioButton_2_clicked\0on_checkBox_clicked\0"
    "on_statusAlarmClock_clicked\0"
    "on_pushButtonParsingCode_clicked\0"
    "on_pushButton_6_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x08 /* Private */,
       5,    1,  102,    2, 0x08 /* Private */,
       6,    1,  105,    2, 0x08 /* Private */,
       7,    0,  108,    2, 0x08 /* Private */,
       8,    0,  109,    2, 0x08 /* Private */,
       9,    0,  110,    2, 0x08 /* Private */,
      10,    0,  111,    2, 0x08 /* Private */,
      11,    0,  112,    2, 0x08 /* Private */,
      12,    0,  113,    2, 0x08 /* Private */,
      13,    0,  114,    2, 0x08 /* Private */,
      14,    1,  115,    2, 0x08 /* Private */,
      17,    0,  118,    2, 0x08 /* Private */,
      18,    0,  119,    2, 0x08 /* Private */,
      19,    0,  120,    2, 0x08 /* Private */,
      20,    0,  121,    2, 0x08 /* Private */,
      21,    0,  122,    2, 0x08 /* Private */,
      22,    0,  123,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->mouseMoveEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 1: _t->mousePressEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 2: _t->mouseReleaseEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 3: _t->loadVarintSearch(); break;
        case 4: _t->writeVarintSearch(); break;
        case 5: _t->blockButton(); break;
        case 6: _t->on_pushButton_clicked(); break;
        case 7: _t->on_pushButton_4_clicked(); break;
        case 8: _t->on_btn_clicked(); break;
        case 9: _t->on_pushButton_5_clicked(); break;
        case 10: _t->on_listWidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 11: _t->on_radioButton_clicked(); break;
        case 12: _t->on_radioButton_2_clicked(); break;
        case 13: _t->on_checkBox_clicked(); break;
        case 14: _t->on_statusAlarmClock_clicked(); break;
        case 15: _t->on_pushButtonParsingCode_clicked(); break;
        case 16: _t->on_pushButton_6_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
