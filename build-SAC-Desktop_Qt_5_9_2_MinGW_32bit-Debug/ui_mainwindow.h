/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDial>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QtQuickWidgets/QQuickWidget"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_5;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout;
    QWidget *tab_2;
    QFrame *frame_2;
    QLabel *label_3;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_5;
    QQuickWidget *quickWidget;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_7;
    QFormLayout *formLayout;
    QLabel *label_2;
    QComboBox *comboBoxFrom;
    QLabel *label_6;
    QComboBox *comboBoxTo;
    QPushButton *btn;
    QPushButton *pushButton;
    QPushButton *pushButton_4;
    QWidget *tab_3;
    QFrame *frame_5;
    QCalendarWidget *calendarWidget;
    QGroupBox *groupBox_5;
    QGroupBox *groupBox_6;
    QListWidget *listWidget;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_7;
    QRadioButton *radioButton;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_4;
    QTimeEdit *timeEdit;
    QGroupBox *groupBox_8;
    QRadioButton *radioButton_2;
    QTimeEdit *timeEdit_2;
    QPushButton *pushButton_5;
    QCheckBox *statusAlarmClock;
    QGroupBox *groupBox_9;
    QDial *dial;
    QLabel *label_5;
    QWidget *tab_4;
    QPushButton *pushButtonParsingCode;
    QWidget *widget;
    QPushButton *pushButton_6;
    QListView *listView;
    QGroupBox *groupBox_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(870, 600);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(870, 600));
        MainWindow->setSizeIncrement(QSize(0, 0));
        MainWindow->setBaseSize(QSize(800, 600));
        MainWindow->setStyleSheet(QLatin1String("\n"
"#MainWindow{\n"
"border:2px solid rgb(85, 59, 34);\n"
"border-radius: 15px;\n"
"padding: 4px;\n"
"margin: 2px;\n"
"background-color:rgb(15, 10, 5);\n"
"}\n"
""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_3 = new QHBoxLayout(centralWidget);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setMinimumSize(QSize(0, 0));
        groupBox_4->setCursor(QCursor(Qt::ClosedHandCursor));
        groupBox_4->setStyleSheet(QLatin1String("#groupBox_4{\n"
"border:2px solid rgb(85, 59, 34);\n"
"border-radius: 15px;\n"
"padding: 4px;\n"
"margin: 2px;\n"
"background-color:rgb(15, 10, 5);\n"
"}\n"
""));
        verticalLayout_2 = new QVBoxLayout(groupBox_4);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(5, 5, 5, 5);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox_2 = new QGroupBox(groupBox_4);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(0, 0));
        groupBox_2->setStyleSheet(QLatin1String("\n"
"#groupBox_2\n"
"{\n"
"border:2px solid black;\n"
"border-top-right-radius: 10px;\n"
"border-top-left-radius: 10px;\n"
"padding: 4px;\n"
"margin: 2px;\n"
"background-color:rgb(67, 47, 27);\n"
"}\n"
""));
        horizontalLayout_5 = new QHBoxLayout(groupBox_2);
        horizontalLayout_5->setSpacing(5);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(5, 0, 5, 0);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(5);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));
        label->setCursor(QCursor(Qt::ArrowCursor));
        label->setStyleSheet(QLatin1String("#label:hover\n"
"{\n"
"     background-color:rgb(106, 74, 43);\n"
"	color:rgb(22, 22, 22);\n"
"}\n"
" "));

        horizontalLayout_4->addWidget(label);

        pushButton_2 = new QPushButton(groupBox_2);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(24, 25));
        pushButton_2->setSizeIncrement(QSize(5, 5));
        pushButton_2->setBaseSize(QSize(5, 5));
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_2->setStyleSheet(QString::fromUtf8("#pushButton_2 {\n"
"   /* border: none;*/\n"
"     background-color: rgb(102, 71, 41);\n"
"	font:bold;\n"
"	color:rgb(0, 0, 0);\n"
"text:\"-\";\n"
"}\n"
"#pushButton_2:hover{\n"
"     background-color:rgb(153, 107, 62);\n"
"color:rgb(22, 22, 22);\n"
"}\n"
" \n"
"/* \320\277\321\200\320\270 \320\275\320\260\320\266\320\260\321\202\320\270\320\270 \320\272\320\275\320\276\320\277\320\272\320\270 */\n"
"#pushButton_2:pressed {\n"
"    background-color: rgb(170, 85, 0);\n"
"border-style: inset;\n"
"color:rgb(0, 0, 0);\n"
"}\n"
"#pushButton_2 \n"
"{\n"
"   /* border: none;*/\n"
"     border-radius:10;\n"
"	padding:4;\n"
"border-width: 2px;\n"
"border-style: outset;\n"
"min-width: 12;\n"
"                                                  \n"
"}"));

        horizontalLayout_4->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(groupBox_2);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(24, 25));
        pushButton_3->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_3->setStyleSheet(QString::fromUtf8("#pushButton_3\n"
" {\n"
"   /* border: none;*/\n"
"     background-color: rgb(190, 0, 0);\n"
"	font:bold;\n"
"	color:rgb(0, 0, 0);\n"
"	text:\"x\";\n"
"}\n"
"#pushButton_3:hover\n"
"{\n"
"     background-color:rgb(217, 0, 0);\n"
"	color:rgb(22, 22, 22);\n"
"}\n"
" \n"
"/* \320\277\321\200\320\270 \320\275\320\260\320\266\320\260\321\202\320\270\320\270 \320\272\320\275\320\276\320\277\320\272\320\270 */\n"
"#pushButton_3:pressed \n"
"{\n"
"    background-color: rgb(255, 0, 0);\n"
"border-style: inset;\n"
"color:rgb(0, 0, 0);\n"
"}\n"
"#pushButton_3 \n"
"{\n"
"   /* border: none;*/\n"
"     border-radius:10;\n"
"	padding:4;\n"
"border-width: 2px;\n"
"border-style: outset;\n"
"min-width: 12;\n"
"                                                  \n"
"}"));

        horizontalLayout_4->addWidget(pushButton_3);

        horizontalLayout_4->setStretch(0, 100);
        horizontalLayout_4->setStretch(1, 1);
        horizontalLayout_4->setStretch(2, 1);

        horizontalLayout_5->addLayout(horizontalLayout_4);


        verticalLayout->addWidget(groupBox_2);

        groupBox = new QGroupBox(groupBox_4);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setStyleSheet(QLatin1String("#groupBox{\n"
"border:2px solid rgb(85, 59, 34);\n"
"padding: 4px;\n"
"margin: 2px;\n"
"background-color:rgb(15, 10, 5);\n"
"}\n"
""));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setSpacing(5);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(5, 5, 5, 5);
        tabWidget = new QTabWidget(groupBox);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setCursor(QCursor(Qt::ArrowCursor));
        tabWidget->setStyleSheet(QStringLiteral(""));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        horizontalLayout_2 = new QHBoxLayout(tab);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));

        horizontalLayout_2->addLayout(horizontalLayout);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        frame_2 = new QFrame(tab_2);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(10, 10, 371, 411));
        frame_2->setStyleSheet(QLatin1String("QFrame\n"
"{\n"
"\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        label_3 = new QLabel(frame_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 10, 351, 21));
        label_3->setStyleSheet(QLatin1String("#label_3\n"
"{\n"
"color:rgb(255, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        layoutWidget = new QWidget(frame_2);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 40, 351, 361));
        verticalLayout_5 = new QVBoxLayout(layoutWidget);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        quickWidget = new QQuickWidget(layoutWidget);
        quickWidget->setObjectName(QStringLiteral("quickWidget"));
        quickWidget->setStyleSheet(QLatin1String("#quickWidget\n"
"{\n"
"color:rgb(255, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
"padding: 1px;\n"
"\n"
"\n"
"}"));
        quickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);

        verticalLayout_5->addWidget(quickWidget);

        layoutWidget1 = new QWidget(tab_2);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(520, 10, 261, 161));
        verticalLayout_6 = new QVBoxLayout(layoutWidget1);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(layoutWidget1);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setStyleSheet(QString::fromUtf8("#label_7\n"
"{\n"
"text: \"\320\237\320\276\320\270\321\201\320\272 \321\200\320\260\321\201\321\201\320\277\320\270\321\201\320\260\320\275\320\270\321\217  \321\202\321\200\320\260\320\275\321\201\320\277\320\276\321\200\321\202\320\260\";\n"
"color: rgb(255, 0, 127);\n"
"image: url(:/rasptrans/buses_gren.svg);\n"
"}"));
        label_7->setTextFormat(Qt::PlainText);
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(label_7);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        comboBoxFrom = new QComboBox(layoutWidget1);
        comboBoxFrom->setObjectName(QStringLiteral("comboBoxFrom"));
        comboBoxFrom->setEditable(true);
        comboBoxFrom->setDuplicatesEnabled(false);

        formLayout->setWidget(0, QFormLayout::FieldRole, comboBoxFrom);

        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_6);

        comboBoxTo = new QComboBox(layoutWidget1);
        comboBoxTo->setObjectName(QStringLiteral("comboBoxTo"));
        comboBoxTo->setEditable(true);
        comboBoxTo->setDuplicatesEnabled(false);

        formLayout->setWidget(1, QFormLayout::FieldRole, comboBoxTo);


        verticalLayout_6->addLayout(formLayout);

        btn = new QPushButton(layoutWidget1);
        btn->setObjectName(QStringLiteral("btn"));
        btn->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_6->addWidget(btn);

        pushButton = new QPushButton(layoutWidget1);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout_6->addWidget(pushButton);

        pushButton_4 = new QPushButton(layoutWidget1);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        verticalLayout_6->addWidget(pushButton_4);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        tab_3->setCursor(QCursor(Qt::PointingHandCursor));
        tab_3->setStyleSheet(QStringLiteral(""));
        frame_5 = new QFrame(tab_3);
        frame_5->setObjectName(QStringLiteral("frame_5"));
        frame_5->setGeometry(QRect(320, 20, 461, 181));
        frame_5->setStyleSheet(QLatin1String("QFrame\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
"border-top-right-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        calendarWidget = new QCalendarWidget(frame_5);
        calendarWidget->setObjectName(QStringLiteral("calendarWidget"));
        calendarWidget->setGeometry(QRect(10, 10, 281, 161));
        calendarWidget->setStyleSheet(QStringLiteral("color:rgb(0, 0, 0)"));
        groupBox_5 = new QGroupBox(tab_3);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(11, 10, 291, 191));
        groupBox_5->setStyleSheet(QLatin1String("#groupBox_5{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-top-left-radius: 10px;\n"
" padding: 1px;\n"
"}"));
        groupBox_6 = new QGroupBox(tab_3);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setGeometry(QRect(10, 210, 297, 216));
        groupBox_6->setStyleSheet(QLatin1String("#groupBox_6{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-bottom-left-radius: 10px;\n"
" padding: 1px;\n"
"}"));
        listWidget = new QListWidget(groupBox_6);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(8, 18, 73, 190));
        listWidget->setStyleSheet(QLatin1String("QListWidget\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 1px solid rgb(58, 0, 0);\n"
" border-bottom-left-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        layoutWidget2 = new QWidget(groupBox_6);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(88, 20, 201, 181));
        verticalLayout_3 = new QVBoxLayout(layoutWidget2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        groupBox_7 = new QGroupBox(layoutWidget2);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setStyleSheet(QLatin1String("QGroupBox{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 1px solid rgb(58, 0, 0);\n"
"\n"
" padding: 1px;\n"
"}"));
        radioButton = new QRadioButton(groupBox_7);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(8, 8, 181, 21));
        radioButton->setBaseSize(QSize(0, 0));
        radioButton->setStyleSheet(QLatin1String("QRadioButton\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        radioButton->setChecked(true);
        radioButton->setAutoRepeatInterval(100);
        layoutWidget3 = new QWidget(groupBox_7);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(10, 40, 181, 31));
        horizontalLayout_6 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget3);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setStyleSheet(QStringLiteral(""));

        horizontalLayout_6->addWidget(label_4);

        timeEdit = new QTimeEdit(layoutWidget3);
        timeEdit->setObjectName(QStringLiteral("timeEdit"));
        timeEdit->setStyleSheet(QLatin1String("QTimeEdit\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        timeEdit->setTime(QTime(1, 0, 0));

        horizontalLayout_6->addWidget(timeEdit);


        verticalLayout_3->addWidget(groupBox_7);

        groupBox_8 = new QGroupBox(layoutWidget2);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        groupBox_8->setStyleSheet(QLatin1String("QGroupBox{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 1px solid rgb(58, 0, 0);\n"
"\n"
" padding: 1px;\n"
"}"));
        radioButton_2 = new QRadioButton(groupBox_8);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setGeometry(QRect(10, 10, 181, 21));
        radioButton_2->setStyleSheet(QLatin1String("QRadioButton\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        timeEdit_2 = new QTimeEdit(groupBox_8);
        timeEdit_2->setObjectName(QStringLiteral("timeEdit_2"));
        timeEdit_2->setGeometry(QRect(14, 40, 81, 22));
        timeEdit_2->setStyleSheet(QLatin1String("QTimeEdit\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));
        pushButton_5 = new QPushButton(groupBox_8);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(100, 40, 91, 21));
        pushButton_5->setStyleSheet(QLatin1String("QPushButton\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-radius: 10px;\n"
" padding: 1px;\n"
"\n"
"\n"
"}"));

        verticalLayout_3->addWidget(groupBox_8);

        statusAlarmClock = new QCheckBox(layoutWidget2);
        statusAlarmClock->setObjectName(QStringLiteral("statusAlarmClock"));

        verticalLayout_3->addWidget(statusAlarmClock);

        groupBox_9 = new QGroupBox(tab_3);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        groupBox_9->setGeometry(QRect(320, 210, 461, 211));
        groupBox_9->setStyleSheet(QLatin1String("#groupBox_9{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
"border: 2px solid rgb(58, 0, 0);\n"
" border-top-left-radius: 10px;\n"
" padding: 1px;\n"
"}"));
        dial = new QDial(groupBox_9);
        dial->setObjectName(QStringLiteral("dial"));
        dial->setGeometry(QRect(10, 60, 131, 121));
        dial->setStyleSheet(QString::fromUtf8("#dial\n"
"{\n"
"color:rgb(85, 0, 127);\n"
"background-color:rgb(41, 0, 62);\n"
"border: 3px solid rgb(255, 0, 127);\n"
" border-radius: 10px;\n"
" padding: 2px;\n"
"text: \"\320\263\321\200\320\276\320\274\320\272\320\276\321\201\321\202\321\214\";\n"
"\n"
"\n"
"}"));
        label_5 = new QLabel(groupBox_9);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(50, 180, 61, 21));
        label_5->setStyleSheet(QLatin1String("#label_5\n"
"{\n"
"color:rgb(0, 0, 0);\n"
"background-color:rgb(175, 169, 126);\n"
" padding: 1px;\n"
"}"));
        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        pushButtonParsingCode = new QPushButton(tab_4);
        pushButtonParsingCode->setObjectName(QStringLiteral("pushButtonParsingCode"));
        pushButtonParsingCode->setGeometry(QRect(270, 20, 111, 31));
        widget = new QWidget(tab_4);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(380, 10, 401, 211));
        pushButton_6 = new QPushButton(tab_4);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(280, 50, 75, 23));
        listView = new QListView(tab_4);
        listView->setObjectName(QStringLiteral("listView"));
        listView->setGeometry(QRect(10, 10, 256, 381));
        tabWidget->addTab(tab_4, QString());

        verticalLayout_4->addWidget(tabWidget);


        verticalLayout->addWidget(groupBox);

        groupBox_3 = new QGroupBox(groupBox_4);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setStyleSheet(QLatin1String("#groupBox_3\n"
"{\n"
"border:2px solid black;\n"
"border-bottom-right-radius: 10px;\n"
"border-bottom-left-radius: 10px;\n"
"padding: 4px;\n"
"margin: 2px;\n"
"background-color:rgb(67, 47, 27);\n"
"}\n"
""));

        verticalLayout->addWidget(groupBox_3);


        verticalLayout_2->addLayout(verticalLayout);


        horizontalLayout_3->addWidget(groupBox_4);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        groupBox_4->setTitle(QString());
        groupBox_2->setTitle(QString());
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/Tray/clock.png\" width=\"15\" height=\"15\"/><span style=\" font-size:11pt; font-weight:600; color:#ff0000;\">S</span><span style=\" font-size:11pt; font-weight:600; color:#ffffff;\">A</span><span style=\" font-size:11pt; font-weight:600; color:#000000;\">C - </span><span style=\" font-size:11pt; font-weight:600; color:#ff0000;\">Student</span><span style=\" font-size:11pt; font-weight:600; color:#000000;\"/><span style=\" font-size:11pt; font-weight:600; color:#ffffff;\">Alarm</span><span style=\" font-size:11pt; font-weight:600; color:#000000;\"> Clock</span></p></body></html>", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainWindow", "-", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("MainWindow", "x", Q_NULLPTR));
        groupBox->setTitle(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "\320\240\320\260\321\201\320\277\320\270\321\201\320\260\320\275\320\270\320\265 \320\277\320\260\321\200", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "\320\237\320\276\320\270\321\201\320\272 \321\200\320\260\321\201\321\201\320\277\320\270\321\201\320\260\320\275\320\270\321\217  \321\202\321\200\320\260\320\275\321\201\320\277\320\276\321\200\321\202\320\260", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "\320\236\321\202\320\272\321\203\320\264\320\260:", Q_NULLPTR));
        comboBoxFrom->clear();
        comboBoxFrom->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "\320\227\320\260\321\200\320\260\320\271\321\201\320\272", Q_NULLPTR)
        );
        label_6->setText(QApplication::translate("MainWindow", "\320\232\321\203\320\264\320\260:", Q_NULLPTR));
        comboBoxTo->clear();
        comboBoxTo->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "\320\232\320\276\320\273\320\276\320\274\320\275\320\260", Q_NULLPTR)
        );
        btn->setText(QApplication::translate("MainWindow", "\321\201\320\272\320\260\321\207\320\260\321\202\321\214", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MainWindow", "\320\277\320\260\321\200\321\201\320\270\320\275\320\263", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MainWindow", "PushButton", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "\320\240\320\260\321\201\320\277\320\270\321\201\320\260\320\275\320\265 \320\260\320\262\321\202\320\276\320\261\321\203\321\201\320\260", Q_NULLPTR));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "\320\275\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270 \321\203\320\262\320\265\320\264\320\276\320\274\320\273\320\265\320\275\320\270\320\271", Q_NULLPTR));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "\320\275\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270 \320\261\321\203\320\264\320\270\320\273\321\214\320\275\320\270\320\272\320\260", Q_NULLPTR));
        groupBox_7->setTitle(QString());
        radioButton->setText(QApplication::translate("MainWindow", "\320\260\320\262\321\202\320\276 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\272\320\260", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\277\320\260\321\201 \320\262\321\200\320\265\320\274\320\275\320\270", Q_NULLPTR));
        timeEdit->setDisplayFormat(QApplication::translate("MainWindow", "HH:mm", Q_NULLPTR));
        groupBox_8->setTitle(QString());
        radioButton_2->setText(QApplication::translate("MainWindow", "\321\200\321\203\321\207\320\275\320\260\321\217 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\272\320\260", Q_NULLPTR));
        timeEdit_2->setDisplayFormat(QApplication::translate("MainWindow", "HH:mm", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("MainWindow", "\321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\270\321\202\321\214", Q_NULLPTR));
        statusAlarmClock->setText(QApplication::translate("MainWindow", "\321\201\321\202\320\260\321\202\321\203\321\201 \320\261\321\203\320\264\320\270\320\273\321\214\320\275\320\270\320\272\320\260", Q_NULLPTR));
        groupBox_9->setTitle(QString());
        label_5->setText(QApplication::translate("MainWindow", "\320\263\321\200\320\276\320\274\320\272\320\276\321\201\321\202\321\214", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "\320\261\321\203\320\264\320\270\320\273\321\214\320\275\320\270\320\272 \320\270 \321\203\320\262\320\265\320\264\320\276\320\274\320\273\320\265\320\275\320\270\321\217", Q_NULLPTR));
        pushButtonParsingCode->setText(QApplication::translate("MainWindow", "parsing", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("MainWindow", "settingObl", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("MainWindow", "\320\241\321\202\321\200\320\260\320\275\320\270\321\206\320\260", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "\320\241\320\265\320\271\321\207\320\260\321\201: ", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
